/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.ServiceContext;

import it.unisef.managesales.model.Firm;
import it.unisef.managesales.model.Worker;
import it.unisef.managesales.service.base.FirmServiceBaseImpl;
import it.unisef.managesales.service.permission.FirmPermission;
import it.unisef.managesales.service.permission.ManageSalesPermission;
import it.unisef.managesales.service.util.ActionKeys;

/**
 * The implementation of the firm remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.unisef.managesales.service.FirmService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Giulio Piemontese
 * @see it.unisef.managesales.service.base.FirmServiceBaseImpl
 * @see it.unisef.managesales.service.FirmServiceUtil
 */
public class FirmServiceImpl extends FirmServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.unisef.managesales.service.FirmServiceUtil} to access the firm remote service.
	 */

	public Firm addFirm(
			long userId, String name, String address,
			ServiceContext context)
			throws PortalException, SystemException {

		ManageSalesPermission.check(getPermissionChecker(), context.getScopeGroupId(), ActionKeys.ADD_FIRM);

		return firmLocalService.addFirm(userId, name, address, context);
	}

	public Firm updateFirm(
			long firmId, String name, String address,
			ServiceContext context)
			throws PortalException, SystemException {

		FirmPermission.check(getPermissionChecker(), firmId, ActionKeys.UPDATE);

		return firmLocalService.updateFirm(firmId, name, address, context);

	}

	public Firm deleteFirm(long firmId)
			throws PortalException, SystemException {

			FirmPermission.check(getPermissionChecker(), firmId, ActionKeys.DELETE);

			return firmLocalService.deleteFirm(firmId);
	}

}