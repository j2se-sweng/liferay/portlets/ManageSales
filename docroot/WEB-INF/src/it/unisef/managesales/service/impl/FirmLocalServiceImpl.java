/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service.impl;

import java.util.Date;
import java.util.List;

import it.unisef.managesales.FirmAddressException;
import it.unisef.managesales.FirmNameException;
import it.unisef.managesales.model.Firm;
import it.unisef.managesales.model.Worker;
import it.unisef.managesales.service.base.FirmLocalServiceBaseImpl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;

/**
 * The implementation of the firm local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.unisef.managesales.service.FirmLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Giulio Piemontese
 * @see it.unisef.managesales.service.base.FirmLocalServiceBaseImpl
 * @see it.unisef.managesales.service.FirmLocalServiceUtil
 */
public class FirmLocalServiceImpl extends FirmLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.unisef.managesales.service.FirmLocalServiceUtil} to access the firm local service.
	 */


	public List<Firm> getFirms(long companyId, long groupId, int start, int end)
					throws SystemException {

		return firmPersistence.findByC_G(companyId, groupId, start, end);
	}

	public List<Firm> listFirms()
			throws SystemException {

		return firmPersistence.findAll();
	}

	public List<Worker> getFirmWorkers(long firmId)
		throws SystemException {

		return workerPersistence.findByFirmId(firmId);
	}

	public int getFirmsCount(long companyId, long groupId)
					throws SystemException {

		return firmPersistence.countByC_G(companyId, groupId);
	}


	public Firm addFirm(
			long userId, String name, String address,
			ServiceContext serviceContext)

		throws PortalException, SystemException {

		User user = userPersistence.findByPrimaryKey(userId);

		validate(name, address);

		long FirmId = counterLocalService.increment();

		Firm firm = firmPersistence.create(FirmId);

		Date now = new Date();

		firm.setCreateDate(now);
		firm.setModifiedDate(now);

		firm.setCompanyId(user.getCompanyId());
		firm.setUserId(user.getUserId());
		firm.setUserName(user.getFullName());

		firm.setGroupId(serviceContext.getScopeGroupId());

		firm.setName(name);
		firm.setAddress(address);


		// La FirmPersistence pu� essere solo richiamata dalla LocalServiceImpl
		firm = firmPersistence.update(firm, false); // false
													// non fa il merge dei dati che eventualmente gi� ci sono

//		resourceLocalService.addModelResources(
//		Firm.getCompanyId(), Firm.getGroupId(), Firm.getUserId(),
//		Firm.class.getName(), Firm.getFirmId(), serviceContext.getGroupPermissions(),
//		serviceContext.getGuestPermissions());


		return firm;
	}

	public Firm updateFirm(
			long firmId, String name, String address,
			ServiceContext context /* nel caso in cui la portlet serva a */
			 /* mostrare degli asset */)
		throws PortalException, SystemException {

		Firm firm = firmPersistence.findByPrimaryKey(firmId);

		validate(name, address);

		firm.setCreateDate(context.getCreateDate());
		firm.setModifiedDate(context.getModifiedDate());

		firm.setName(name);
		firm.setAddress(address);

		firm = firmPersistence.update(firm, false);

		return firm;

	}

	public Firm deleteFirm(long firmId)
		throws PortalException, SystemException {

		return firmPersistence.remove(firmId);
	}

	public void deleteFirmWorkers(long firmId)
		throws PortalException, SystemException {

		List<Worker> firmWorkers = workerPersistence.findByFirmId(firmId);

		for(Worker w : firmWorkers) {
			workerPersistence.remove(w);
		}

	}
	
	

	protected void validate(String name, String address) throws PortalException {

		if(Validator.isNull(name)) {
			throw new FirmNameException();
		}

		if(Validator.isNull(name)) {
			throw new FirmAddressException();
		}

	}

}