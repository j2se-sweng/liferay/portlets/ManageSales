/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.security.permission.InlineSQLHelperUtil;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.unisef.managesales.NoSuchFirmException;
import it.unisef.managesales.model.Firm;
import it.unisef.managesales.model.impl.FirmImpl;
import it.unisef.managesales.model.impl.FirmModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the firm service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Giulio Piemontese
 * @see FirmPersistence
 * @see FirmUtil
 * @generated
 */
public class FirmPersistenceImpl extends BasePersistenceImpl<Firm>
	implements FirmPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link FirmUtil} to access the firm persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = FirmImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C_G = new FinderPath(FirmModelImpl.ENTITY_CACHE_ENABLED,
			FirmModelImpl.FINDER_CACHE_ENABLED, FirmImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByC_G",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G = new FinderPath(FirmModelImpl.ENTITY_CACHE_ENABLED,
			FirmModelImpl.FINDER_CACHE_ENABLED, FirmImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByC_G",
			new String[] { Long.class.getName(), Long.class.getName() },
			FirmModelImpl.COMPANYID_COLUMN_BITMASK |
			FirmModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_G = new FinderPath(FirmModelImpl.ENTITY_CACHE_ENABLED,
			FirmModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_G",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(FirmModelImpl.ENTITY_CACHE_ENABLED,
			FirmModelImpl.FINDER_CACHE_ENABLED, FirmImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(FirmModelImpl.ENTITY_CACHE_ENABLED,
			FirmModelImpl.FINDER_CACHE_ENABLED, FirmImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(FirmModelImpl.ENTITY_CACHE_ENABLED,
			FirmModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the firm in the entity cache if it is enabled.
	 *
	 * @param firm the firm
	 */
	public void cacheResult(Firm firm) {
		EntityCacheUtil.putResult(FirmModelImpl.ENTITY_CACHE_ENABLED,
			FirmImpl.class, firm.getPrimaryKey(), firm);

		firm.resetOriginalValues();
	}

	/**
	 * Caches the firms in the entity cache if it is enabled.
	 *
	 * @param firms the firms
	 */
	public void cacheResult(List<Firm> firms) {
		for (Firm firm : firms) {
			if (EntityCacheUtil.getResult(FirmModelImpl.ENTITY_CACHE_ENABLED,
						FirmImpl.class, firm.getPrimaryKey()) == null) {
				cacheResult(firm);
			}
			else {
				firm.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all firms.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(FirmImpl.class.getName());
		}

		EntityCacheUtil.clearCache(FirmImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the firm.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Firm firm) {
		EntityCacheUtil.removeResult(FirmModelImpl.ENTITY_CACHE_ENABLED,
			FirmImpl.class, firm.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Firm> firms) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Firm firm : firms) {
			EntityCacheUtil.removeResult(FirmModelImpl.ENTITY_CACHE_ENABLED,
				FirmImpl.class, firm.getPrimaryKey());
		}
	}

	/**
	 * Creates a new firm with the primary key. Does not add the firm to the database.
	 *
	 * @param firmId the primary key for the new firm
	 * @return the new firm
	 */
	public Firm create(long firmId) {
		Firm firm = new FirmImpl();

		firm.setNew(true);
		firm.setPrimaryKey(firmId);

		return firm;
	}

	/**
	 * Removes the firm with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param firmId the primary key of the firm
	 * @return the firm that was removed
	 * @throws it.unisef.managesales.NoSuchFirmException if a firm with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Firm remove(long firmId) throws NoSuchFirmException, SystemException {
		return remove(Long.valueOf(firmId));
	}

	/**
	 * Removes the firm with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the firm
	 * @return the firm that was removed
	 * @throws it.unisef.managesales.NoSuchFirmException if a firm with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Firm remove(Serializable primaryKey)
		throws NoSuchFirmException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Firm firm = (Firm)session.get(FirmImpl.class, primaryKey);

			if (firm == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchFirmException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(firm);
		}
		catch (NoSuchFirmException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Firm removeImpl(Firm firm) throws SystemException {
		firm = toUnwrappedModel(firm);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, firm);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(firm);

		return firm;
	}

	@Override
	public Firm updateImpl(it.unisef.managesales.model.Firm firm, boolean merge)
		throws SystemException {
		firm = toUnwrappedModel(firm);

		boolean isNew = firm.isNew();

		FirmModelImpl firmModelImpl = (FirmModelImpl)firm;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, firm, merge);

			firm.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !FirmModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((firmModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(firmModelImpl.getOriginalCompanyId()),
						Long.valueOf(firmModelImpl.getOriginalGroupId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_G, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G,
					args);

				args = new Object[] {
						Long.valueOf(firmModelImpl.getCompanyId()),
						Long.valueOf(firmModelImpl.getGroupId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_G, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G,
					args);
			}
		}

		EntityCacheUtil.putResult(FirmModelImpl.ENTITY_CACHE_ENABLED,
			FirmImpl.class, firm.getPrimaryKey(), firm);

		return firm;
	}

	protected Firm toUnwrappedModel(Firm firm) {
		if (firm instanceof FirmImpl) {
			return firm;
		}

		FirmImpl firmImpl = new FirmImpl();

		firmImpl.setNew(firm.isNew());
		firmImpl.setPrimaryKey(firm.getPrimaryKey());

		firmImpl.setFirmId(firm.getFirmId());
		firmImpl.setCompanyId(firm.getCompanyId());
		firmImpl.setUserId(firm.getUserId());
		firmImpl.setUserName(firm.getUserName());
		firmImpl.setCreateDate(firm.getCreateDate());
		firmImpl.setModifiedDate(firm.getModifiedDate());
		firmImpl.setGroupId(firm.getGroupId());
		firmImpl.setName(firm.getName());
		firmImpl.setAddress(firm.getAddress());

		return firmImpl;
	}

	/**
	 * Returns the firm with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the firm
	 * @return the firm
	 * @throws com.liferay.portal.NoSuchModelException if a firm with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Firm findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the firm with the primary key or throws a {@link it.unisef.managesales.NoSuchFirmException} if it could not be found.
	 *
	 * @param firmId the primary key of the firm
	 * @return the firm
	 * @throws it.unisef.managesales.NoSuchFirmException if a firm with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Firm findByPrimaryKey(long firmId)
		throws NoSuchFirmException, SystemException {
		Firm firm = fetchByPrimaryKey(firmId);

		if (firm == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + firmId);
			}

			throw new NoSuchFirmException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				firmId);
		}

		return firm;
	}

	/**
	 * Returns the firm with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the firm
	 * @return the firm, or <code>null</code> if a firm with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Firm fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the firm with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param firmId the primary key of the firm
	 * @return the firm, or <code>null</code> if a firm with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Firm fetchByPrimaryKey(long firmId) throws SystemException {
		Firm firm = (Firm)EntityCacheUtil.getResult(FirmModelImpl.ENTITY_CACHE_ENABLED,
				FirmImpl.class, firmId);

		if (firm == _nullFirm) {
			return null;
		}

		if (firm == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				firm = (Firm)session.get(FirmImpl.class, Long.valueOf(firmId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (firm != null) {
					cacheResult(firm);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(FirmModelImpl.ENTITY_CACHE_ENABLED,
						FirmImpl.class, firmId, _nullFirm);
				}

				closeSession(session);
			}
		}

		return firm;
	}

	/**
	 * Returns all the firms where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching firms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Firm> findByC_G(long companyId, long groupId)
		throws SystemException {
		return findByC_G(companyId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the firms where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of firms
	 * @param end the upper bound of the range of firms (not inclusive)
	 * @return the range of matching firms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Firm> findByC_G(long companyId, long groupId, int start, int end)
		throws SystemException {
		return findByC_G(companyId, groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the firms where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of firms
	 * @param end the upper bound of the range of firms (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching firms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Firm> findByC_G(long companyId, long groupId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G;
			finderArgs = new Object[] { companyId, groupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C_G;
			finderArgs = new Object[] {
					companyId, groupId,
					
					start, end, orderByComparator
				};
		}

		List<Firm> list = (List<Firm>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Firm firm : list) {
				if ((companyId != firm.getCompanyId()) ||
						(groupId != firm.getGroupId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_FIRM_WHERE);

			query.append(_FINDER_COLUMN_C_G_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_G_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(FirmModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(groupId);

				list = (List<Firm>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first firm in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching firm
	 * @throws it.unisef.managesales.NoSuchFirmException if a matching firm could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Firm findByC_G_First(long companyId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchFirmException, SystemException {
		Firm firm = fetchByC_G_First(companyId, groupId, orderByComparator);

		if (firm != null) {
			return firm;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchFirmException(msg.toString());
	}

	/**
	 * Returns the first firm in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching firm, or <code>null</code> if a matching firm could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Firm fetchByC_G_First(long companyId, long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Firm> list = findByC_G(companyId, groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last firm in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching firm
	 * @throws it.unisef.managesales.NoSuchFirmException if a matching firm could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Firm findByC_G_Last(long companyId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchFirmException, SystemException {
		Firm firm = fetchByC_G_Last(companyId, groupId, orderByComparator);

		if (firm != null) {
			return firm;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchFirmException(msg.toString());
	}

	/**
	 * Returns the last firm in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching firm, or <code>null</code> if a matching firm could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Firm fetchByC_G_Last(long companyId, long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByC_G(companyId, groupId);

		List<Firm> list = findByC_G(companyId, groupId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the firms before and after the current firm in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param firmId the primary key of the current firm
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next firm
	 * @throws it.unisef.managesales.NoSuchFirmException if a firm with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Firm[] findByC_G_PrevAndNext(long firmId, long companyId,
		long groupId, OrderByComparator orderByComparator)
		throws NoSuchFirmException, SystemException {
		Firm firm = findByPrimaryKey(firmId);

		Session session = null;

		try {
			session = openSession();

			Firm[] array = new FirmImpl[3];

			array[0] = getByC_G_PrevAndNext(session, firm, companyId, groupId,
					orderByComparator, true);

			array[1] = firm;

			array[2] = getByC_G_PrevAndNext(session, firm, companyId, groupId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Firm getByC_G_PrevAndNext(Session session, Firm firm,
		long companyId, long groupId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_FIRM_WHERE);

		query.append(_FINDER_COLUMN_C_G_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_G_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(FirmModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(firm);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Firm> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the firms that the user has permission to view where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching firms that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<Firm> filterFindByC_G(long companyId, long groupId)
		throws SystemException {
		return filterFindByC_G(companyId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the firms that the user has permission to view where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of firms
	 * @param end the upper bound of the range of firms (not inclusive)
	 * @return the range of matching firms that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<Firm> filterFindByC_G(long companyId, long groupId, int start,
		int end) throws SystemException {
		return filterFindByC_G(companyId, groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the firms that the user has permissions to view where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of firms
	 * @param end the upper bound of the range of firms (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching firms that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<Firm> filterFindByC_G(long companyId, long groupId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByC_G(companyId, groupId, start, end, orderByComparator);
		}

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_FIRM_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_FIRM_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_C_G_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_G_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_FIRM_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_TABLE,
					orderByComparator);
			}
		}

		else {
			if (getDB().isSupportsInlineDistinct()) {
				query.append(FirmModelImpl.ORDER_BY_JPQL);
			}
			else {
				query.append(FirmModelImpl.ORDER_BY_SQL);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				Firm.class.getName(), _FILTER_ENTITY_TABLE_FILTER_PK_COLUMN,
				groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				q.addEntity(_FILTER_ENTITY_ALIAS, FirmImpl.class);
			}
			else {
				q.addEntity(_FILTER_ENTITY_TABLE, FirmImpl.class);
			}

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(companyId);

			qPos.add(groupId);

			return (List<Firm>)QueryUtil.list(q, getDialect(), start, end);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Returns the firms before and after the current firm in the ordered set of firms that the user has permission to view where companyId = &#63; and groupId = &#63;.
	 *
	 * @param firmId the primary key of the current firm
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next firm
	 * @throws it.unisef.managesales.NoSuchFirmException if a firm with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Firm[] filterFindByC_G_PrevAndNext(long firmId, long companyId,
		long groupId, OrderByComparator orderByComparator)
		throws NoSuchFirmException, SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByC_G_PrevAndNext(firmId, companyId, groupId,
				orderByComparator);
		}

		Firm firm = findByPrimaryKey(firmId);

		Session session = null;

		try {
			session = openSession();

			Firm[] array = new FirmImpl[3];

			array[0] = filterGetByC_G_PrevAndNext(session, firm, companyId,
					groupId, orderByComparator, true);

			array[1] = firm;

			array[2] = filterGetByC_G_PrevAndNext(session, firm, companyId,
					groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Firm filterGetByC_G_PrevAndNext(Session session, Firm firm,
		long companyId, long groupId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_FIRM_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_FIRM_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_C_G_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_G_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_FIRM_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				if (getDB().isSupportsInlineDistinct()) {
					query.append(_ORDER_BY_ENTITY_ALIAS);
				}
				else {
					query.append(_ORDER_BY_ENTITY_TABLE);
				}

				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				if (getDB().isSupportsInlineDistinct()) {
					query.append(_ORDER_BY_ENTITY_ALIAS);
				}
				else {
					query.append(_ORDER_BY_ENTITY_TABLE);
				}

				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			if (getDB().isSupportsInlineDistinct()) {
				query.append(FirmModelImpl.ORDER_BY_JPQL);
			}
			else {
				query.append(FirmModelImpl.ORDER_BY_SQL);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				Firm.class.getName(), _FILTER_ENTITY_TABLE_FILTER_PK_COLUMN,
				groupId);

		SQLQuery q = session.createSQLQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		if (getDB().isSupportsInlineDistinct()) {
			q.addEntity(_FILTER_ENTITY_ALIAS, FirmImpl.class);
		}
		else {
			q.addEntity(_FILTER_ENTITY_TABLE, FirmImpl.class);
		}

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(firm);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Firm> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the firms.
	 *
	 * @return the firms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Firm> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the firms.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of firms
	 * @param end the upper bound of the range of firms (not inclusive)
	 * @return the range of firms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Firm> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the firms.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of firms
	 * @param end the upper bound of the range of firms (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of firms
	 * @throws SystemException if a system exception occurred
	 */
	public List<Firm> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Firm> list = (List<Firm>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_FIRM);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_FIRM.concat(FirmModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Firm>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Firm>)QueryUtil.list(q, getDialect(), start,
							end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the firms where companyId = &#63; and groupId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByC_G(long companyId, long groupId)
		throws SystemException {
		for (Firm firm : findByC_G(companyId, groupId)) {
			remove(firm);
		}
	}

	/**
	 * Removes all the firms from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Firm firm : findAll()) {
			remove(firm);
		}
	}

	/**
	 * Returns the number of firms where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching firms
	 * @throws SystemException if a system exception occurred
	 */
	public int countByC_G(long companyId, long groupId)
		throws SystemException {
		Object[] finderArgs = new Object[] { companyId, groupId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_C_G,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_FIRM_WHERE);

			query.append(_FINDER_COLUMN_C_G_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_G, finderArgs,
					count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of firms that the user has permission to view where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching firms that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public int filterCountByC_G(long companyId, long groupId)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByC_G(companyId, groupId);
		}

		StringBundler query = new StringBundler(3);

		query.append(_FILTER_SQL_COUNT_FIRM_WHERE);

		query.append(_FINDER_COLUMN_C_G_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_G_GROUPID_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				Firm.class.getName(), _FILTER_ENTITY_TABLE_FILTER_PK_COLUMN,
				groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			q.addScalar(COUNT_COLUMN_NAME,
				com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(companyId);

			qPos.add(groupId);

			Long count = (Long)q.uniqueResult();

			return count.intValue();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Returns the number of firms.
	 *
	 * @return the number of firms
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_FIRM);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the firm persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.unisef.managesales.model.Firm")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Firm>> listenersList = new ArrayList<ModelListener<Firm>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Firm>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(FirmImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = FirmPersistence.class)
	protected FirmPersistence firmPersistence;
	@BeanReference(type = WorkerPersistence.class)
	protected WorkerPersistence workerPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_FIRM = "SELECT firm FROM Firm firm";
	private static final String _SQL_SELECT_FIRM_WHERE = "SELECT firm FROM Firm firm WHERE ";
	private static final String _SQL_COUNT_FIRM = "SELECT COUNT(firm) FROM Firm firm";
	private static final String _SQL_COUNT_FIRM_WHERE = "SELECT COUNT(firm) FROM Firm firm WHERE ";
	private static final String _FINDER_COLUMN_C_G_COMPANYID_2 = "firm.companyId = ? AND ";
	private static final String _FINDER_COLUMN_C_G_GROUPID_2 = "firm.groupId = ?";
	private static final String _FILTER_ENTITY_TABLE_FILTER_PK_COLUMN = "firm.firmId";
	private static final String _FILTER_SQL_SELECT_FIRM_WHERE = "SELECT DISTINCT {firm.*} FROM MS_Firm firm WHERE ";
	private static final String _FILTER_SQL_SELECT_FIRM_NO_INLINE_DISTINCT_WHERE_1 =
		"SELECT {MS_Firm.*} FROM (SELECT DISTINCT firm.firmId FROM MS_Firm firm WHERE ";
	private static final String _FILTER_SQL_SELECT_FIRM_NO_INLINE_DISTINCT_WHERE_2 =
		") TEMP_TABLE INNER JOIN MS_Firm ON TEMP_TABLE.firmId = MS_Firm.firmId";
	private static final String _FILTER_SQL_COUNT_FIRM_WHERE = "SELECT COUNT(DISTINCT firm.firmId) AS COUNT_VALUE FROM MS_Firm firm WHERE ";
	private static final String _FILTER_ENTITY_ALIAS = "firm";
	private static final String _FILTER_ENTITY_TABLE = "MS_Firm";
	private static final String _ORDER_BY_ENTITY_ALIAS = "firm.";
	private static final String _ORDER_BY_ENTITY_TABLE = "MS_Firm.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Firm exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Firm exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(FirmPersistenceImpl.class);
	private static Firm _nullFirm = new FirmImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Firm> toCacheModel() {
				return _nullFirmCacheModel;
			}
		};

	private static CacheModel<Firm> _nullFirmCacheModel = new CacheModel<Firm>() {
			public Firm toEntityModel() {
				return _nullFirm;
			}
		};
}