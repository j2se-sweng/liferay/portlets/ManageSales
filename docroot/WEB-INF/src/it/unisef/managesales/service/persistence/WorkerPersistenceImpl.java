/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service.persistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.security.permission.InlineSQLHelperUtil;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.unisef.managesales.NoSuchWorkerException;
import it.unisef.managesales.model.Worker;
import it.unisef.managesales.model.impl.WorkerImpl;
import it.unisef.managesales.model.impl.WorkerModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the worker service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Giulio Piemontese
 * @see WorkerPersistence
 * @see WorkerUtil
 * @generated
 */
public class WorkerPersistenceImpl extends BasePersistenceImpl<Worker>
	implements WorkerPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link WorkerUtil} to access the worker persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = WorkerImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_C_G = new FinderPath(WorkerModelImpl.ENTITY_CACHE_ENABLED,
			WorkerModelImpl.FINDER_CACHE_ENABLED, WorkerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByC_G",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G = new FinderPath(WorkerModelImpl.ENTITY_CACHE_ENABLED,
			WorkerModelImpl.FINDER_CACHE_ENABLED, WorkerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByC_G",
			new String[] { Long.class.getName(), Long.class.getName() },
			WorkerModelImpl.COMPANYID_COLUMN_BITMASK |
			WorkerModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_C_G = new FinderPath(WorkerModelImpl.ENTITY_CACHE_ENABLED,
			WorkerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByC_G",
			new String[] { Long.class.getName(), Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FIRMID = new FinderPath(WorkerModelImpl.ENTITY_CACHE_ENABLED,
			WorkerModelImpl.FINDER_CACHE_ENABLED, WorkerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByFirmId",
			new String[] {
				Long.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FIRMID =
		new FinderPath(WorkerModelImpl.ENTITY_CACHE_ENABLED,
			WorkerModelImpl.FINDER_CACHE_ENABLED, WorkerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByFirmId",
			new String[] { Long.class.getName() },
			WorkerModelImpl.FIRMID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FIRMID = new FinderPath(WorkerModelImpl.ENTITY_CACHE_ENABLED,
			WorkerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByFirmId",
			new String[] { Long.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(WorkerModelImpl.ENTITY_CACHE_ENABLED,
			WorkerModelImpl.FINDER_CACHE_ENABLED, WorkerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(WorkerModelImpl.ENTITY_CACHE_ENABLED,
			WorkerModelImpl.FINDER_CACHE_ENABLED, WorkerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(WorkerModelImpl.ENTITY_CACHE_ENABLED,
			WorkerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the worker in the entity cache if it is enabled.
	 *
	 * @param worker the worker
	 */
	public void cacheResult(Worker worker) {
		EntityCacheUtil.putResult(WorkerModelImpl.ENTITY_CACHE_ENABLED,
			WorkerImpl.class, worker.getPrimaryKey(), worker);

		worker.resetOriginalValues();
	}

	/**
	 * Caches the workers in the entity cache if it is enabled.
	 *
	 * @param workers the workers
	 */
	public void cacheResult(List<Worker> workers) {
		for (Worker worker : workers) {
			if (EntityCacheUtil.getResult(
						WorkerModelImpl.ENTITY_CACHE_ENABLED, WorkerImpl.class,
						worker.getPrimaryKey()) == null) {
				cacheResult(worker);
			}
			else {
				worker.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all workers.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(WorkerImpl.class.getName());
		}

		EntityCacheUtil.clearCache(WorkerImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the worker.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Worker worker) {
		EntityCacheUtil.removeResult(WorkerModelImpl.ENTITY_CACHE_ENABLED,
			WorkerImpl.class, worker.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Worker> workers) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Worker worker : workers) {
			EntityCacheUtil.removeResult(WorkerModelImpl.ENTITY_CACHE_ENABLED,
				WorkerImpl.class, worker.getPrimaryKey());
		}
	}

	/**
	 * Creates a new worker with the primary key. Does not add the worker to the database.
	 *
	 * @param workerId the primary key for the new worker
	 * @return the new worker
	 */
	public Worker create(long workerId) {
		Worker worker = new WorkerImpl();

		worker.setNew(true);
		worker.setPrimaryKey(workerId);

		return worker;
	}

	/**
	 * Removes the worker with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param workerId the primary key of the worker
	 * @return the worker that was removed
	 * @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Worker remove(long workerId)
		throws NoSuchWorkerException, SystemException {
		return remove(Long.valueOf(workerId));
	}

	/**
	 * Removes the worker with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the worker
	 * @return the worker that was removed
	 * @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Worker remove(Serializable primaryKey)
		throws NoSuchWorkerException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Worker worker = (Worker)session.get(WorkerImpl.class, primaryKey);

			if (worker == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchWorkerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(worker);
		}
		catch (NoSuchWorkerException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Worker removeImpl(Worker worker) throws SystemException {
		worker = toUnwrappedModel(worker);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, worker);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(worker);

		return worker;
	}

	@Override
	public Worker updateImpl(it.unisef.managesales.model.Worker worker,
		boolean merge) throws SystemException {
		worker = toUnwrappedModel(worker);

		boolean isNew = worker.isNew();

		WorkerModelImpl workerModelImpl = (WorkerModelImpl)worker;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, worker, merge);

			worker.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !WorkerModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((workerModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(workerModelImpl.getOriginalCompanyId()),
						Long.valueOf(workerModelImpl.getOriginalGroupId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_G, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G,
					args);

				args = new Object[] {
						Long.valueOf(workerModelImpl.getCompanyId()),
						Long.valueOf(workerModelImpl.getGroupId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_C_G, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G,
					args);
			}

			if ((workerModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FIRMID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						Long.valueOf(workerModelImpl.getOriginalFirmId())
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FIRMID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FIRMID,
					args);

				args = new Object[] { Long.valueOf(workerModelImpl.getFirmId()) };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FIRMID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FIRMID,
					args);
			}
		}

		EntityCacheUtil.putResult(WorkerModelImpl.ENTITY_CACHE_ENABLED,
			WorkerImpl.class, worker.getPrimaryKey(), worker);

		return worker;
	}

	protected Worker toUnwrappedModel(Worker worker) {
		if (worker instanceof WorkerImpl) {
			return worker;
		}

		WorkerImpl workerImpl = new WorkerImpl();

		workerImpl.setNew(worker.isNew());
		workerImpl.setPrimaryKey(worker.getPrimaryKey());

		workerImpl.setWorkerId(worker.getWorkerId());
		workerImpl.setCompanyId(worker.getCompanyId());
		workerImpl.setUserId(worker.getUserId());
		workerImpl.setUserName(worker.getUserName());
		workerImpl.setCreateDate(worker.getCreateDate());
		workerImpl.setModifiedDate(worker.getModifiedDate());
		workerImpl.setGroupId(worker.getGroupId());
		workerImpl.setFirstName(worker.getFirstName());
		workerImpl.setLastName(worker.getLastName());
		workerImpl.setFirmId(worker.getFirmId());
		workerImpl.setEmploymentDate(worker.getEmploymentDate());
		workerImpl.setCode(worker.getCode());

		return workerImpl;
	}

	/**
	 * Returns the worker with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the worker
	 * @return the worker
	 * @throws com.liferay.portal.NoSuchModelException if a worker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Worker findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the worker with the primary key or throws a {@link it.unisef.managesales.NoSuchWorkerException} if it could not be found.
	 *
	 * @param workerId the primary key of the worker
	 * @return the worker
	 * @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Worker findByPrimaryKey(long workerId)
		throws NoSuchWorkerException, SystemException {
		Worker worker = fetchByPrimaryKey(workerId);

		if (worker == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + workerId);
			}

			throw new NoSuchWorkerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				workerId);
		}

		return worker;
	}

	/**
	 * Returns the worker with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the worker
	 * @return the worker, or <code>null</code> if a worker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Worker fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the worker with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param workerId the primary key of the worker
	 * @return the worker, or <code>null</code> if a worker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Worker fetchByPrimaryKey(long workerId) throws SystemException {
		Worker worker = (Worker)EntityCacheUtil.getResult(WorkerModelImpl.ENTITY_CACHE_ENABLED,
				WorkerImpl.class, workerId);

		if (worker == _nullWorker) {
			return null;
		}

		if (worker == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				worker = (Worker)session.get(WorkerImpl.class,
						Long.valueOf(workerId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (worker != null) {
					cacheResult(worker);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(WorkerModelImpl.ENTITY_CACHE_ENABLED,
						WorkerImpl.class, workerId, _nullWorker);
				}

				closeSession(session);
			}
		}

		return worker;
	}

	/**
	 * Returns all the workers where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching workers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Worker> findByC_G(long companyId, long groupId)
		throws SystemException {
		return findByC_G(companyId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the workers where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of workers
	 * @param end the upper bound of the range of workers (not inclusive)
	 * @return the range of matching workers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Worker> findByC_G(long companyId, long groupId, int start,
		int end) throws SystemException {
		return findByC_G(companyId, groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the workers where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of workers
	 * @param end the upper bound of the range of workers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching workers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Worker> findByC_G(long companyId, long groupId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_C_G;
			finderArgs = new Object[] { companyId, groupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_C_G;
			finderArgs = new Object[] {
					companyId, groupId,
					
					start, end, orderByComparator
				};
		}

		List<Worker> list = (List<Worker>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Worker worker : list) {
				if ((companyId != worker.getCompanyId()) ||
						(groupId != worker.getGroupId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_WORKER_WHERE);

			query.append(_FINDER_COLUMN_C_G_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_G_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(WorkerModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(groupId);

				list = (List<Worker>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first worker in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching worker
	 * @throws it.unisef.managesales.NoSuchWorkerException if a matching worker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Worker findByC_G_First(long companyId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchWorkerException, SystemException {
		Worker worker = fetchByC_G_First(companyId, groupId, orderByComparator);

		if (worker != null) {
			return worker;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWorkerException(msg.toString());
	}

	/**
	 * Returns the first worker in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching worker, or <code>null</code> if a matching worker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Worker fetchByC_G_First(long companyId, long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Worker> list = findByC_G(companyId, groupId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last worker in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching worker
	 * @throws it.unisef.managesales.NoSuchWorkerException if a matching worker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Worker findByC_G_Last(long companyId, long groupId,
		OrderByComparator orderByComparator)
		throws NoSuchWorkerException, SystemException {
		Worker worker = fetchByC_G_Last(companyId, groupId, orderByComparator);

		if (worker != null) {
			return worker;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWorkerException(msg.toString());
	}

	/**
	 * Returns the last worker in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching worker, or <code>null</code> if a matching worker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Worker fetchByC_G_Last(long companyId, long groupId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByC_G(companyId, groupId);

		List<Worker> list = findByC_G(companyId, groupId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the workers before and after the current worker in the ordered set where companyId = &#63; and groupId = &#63;.
	 *
	 * @param workerId the primary key of the current worker
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next worker
	 * @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Worker[] findByC_G_PrevAndNext(long workerId, long companyId,
		long groupId, OrderByComparator orderByComparator)
		throws NoSuchWorkerException, SystemException {
		Worker worker = findByPrimaryKey(workerId);

		Session session = null;

		try {
			session = openSession();

			Worker[] array = new WorkerImpl[3];

			array[0] = getByC_G_PrevAndNext(session, worker, companyId,
					groupId, orderByComparator, true);

			array[1] = worker;

			array[2] = getByC_G_PrevAndNext(session, worker, companyId,
					groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Worker getByC_G_PrevAndNext(Session session, Worker worker,
		long companyId, long groupId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_WORKER_WHERE);

		query.append(_FINDER_COLUMN_C_G_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_G_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(WorkerModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(worker);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Worker> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the workers that the user has permission to view where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the matching workers that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<Worker> filterFindByC_G(long companyId, long groupId)
		throws SystemException {
		return filterFindByC_G(companyId, groupId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the workers that the user has permission to view where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of workers
	 * @param end the upper bound of the range of workers (not inclusive)
	 * @return the range of matching workers that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<Worker> filterFindByC_G(long companyId, long groupId,
		int start, int end) throws SystemException {
		return filterFindByC_G(companyId, groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the workers that the user has permissions to view where companyId = &#63; and groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param start the lower bound of the range of workers
	 * @param end the upper bound of the range of workers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching workers that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public List<Worker> filterFindByC_G(long companyId, long groupId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByC_G(companyId, groupId, start, end, orderByComparator);
		}

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_WORKER_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_WORKER_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_C_G_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_G_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_WORKER_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			if (getDB().isSupportsInlineDistinct()) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_TABLE,
					orderByComparator);
			}
		}

		else {
			if (getDB().isSupportsInlineDistinct()) {
				query.append(WorkerModelImpl.ORDER_BY_JPQL);
			}
			else {
				query.append(WorkerModelImpl.ORDER_BY_SQL);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				Worker.class.getName(), _FILTER_ENTITY_TABLE_FILTER_PK_COLUMN,
				groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			if (getDB().isSupportsInlineDistinct()) {
				q.addEntity(_FILTER_ENTITY_ALIAS, WorkerImpl.class);
			}
			else {
				q.addEntity(_FILTER_ENTITY_TABLE, WorkerImpl.class);
			}

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(companyId);

			qPos.add(groupId);

			return (List<Worker>)QueryUtil.list(q, getDialect(), start, end);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Returns the workers before and after the current worker in the ordered set of workers that the user has permission to view where companyId = &#63; and groupId = &#63;.
	 *
	 * @param workerId the primary key of the current worker
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next worker
	 * @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Worker[] filterFindByC_G_PrevAndNext(long workerId, long companyId,
		long groupId, OrderByComparator orderByComparator)
		throws NoSuchWorkerException, SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return findByC_G_PrevAndNext(workerId, companyId, groupId,
				orderByComparator);
		}

		Worker worker = findByPrimaryKey(workerId);

		Session session = null;

		try {
			session = openSession();

			Worker[] array = new WorkerImpl[3];

			array[0] = filterGetByC_G_PrevAndNext(session, worker, companyId,
					groupId, orderByComparator, true);

			array[1] = worker;

			array[2] = filterGetByC_G_PrevAndNext(session, worker, companyId,
					groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Worker filterGetByC_G_PrevAndNext(Session session, Worker worker,
		long companyId, long groupId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		if (getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_WORKER_WHERE);
		}
		else {
			query.append(_FILTER_SQL_SELECT_WORKER_NO_INLINE_DISTINCT_WHERE_1);
		}

		query.append(_FINDER_COLUMN_C_G_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_G_GROUPID_2);

		if (!getDB().isSupportsInlineDistinct()) {
			query.append(_FILTER_SQL_SELECT_WORKER_NO_INLINE_DISTINCT_WHERE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				if (getDB().isSupportsInlineDistinct()) {
					query.append(_ORDER_BY_ENTITY_ALIAS);
				}
				else {
					query.append(_ORDER_BY_ENTITY_TABLE);
				}

				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				if (getDB().isSupportsInlineDistinct()) {
					query.append(_ORDER_BY_ENTITY_ALIAS);
				}
				else {
					query.append(_ORDER_BY_ENTITY_TABLE);
				}

				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			if (getDB().isSupportsInlineDistinct()) {
				query.append(WorkerModelImpl.ORDER_BY_JPQL);
			}
			else {
				query.append(WorkerModelImpl.ORDER_BY_SQL);
			}
		}

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				Worker.class.getName(), _FILTER_ENTITY_TABLE_FILTER_PK_COLUMN,
				groupId);

		SQLQuery q = session.createSQLQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		if (getDB().isSupportsInlineDistinct()) {
			q.addEntity(_FILTER_ENTITY_ALIAS, WorkerImpl.class);
		}
		else {
			q.addEntity(_FILTER_ENTITY_TABLE, WorkerImpl.class);
		}

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(worker);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Worker> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the workers where firmId = &#63;.
	 *
	 * @param firmId the firm ID
	 * @return the matching workers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Worker> findByFirmId(long firmId) throws SystemException {
		return findByFirmId(firmId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the workers where firmId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param firmId the firm ID
	 * @param start the lower bound of the range of workers
	 * @param end the upper bound of the range of workers (not inclusive)
	 * @return the range of matching workers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Worker> findByFirmId(long firmId, int start, int end)
		throws SystemException {
		return findByFirmId(firmId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the workers where firmId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param firmId the firm ID
	 * @param start the lower bound of the range of workers
	 * @param end the upper bound of the range of workers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching workers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Worker> findByFirmId(long firmId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FIRMID;
			finderArgs = new Object[] { firmId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FIRMID;
			finderArgs = new Object[] { firmId, start, end, orderByComparator };
		}

		List<Worker> list = (List<Worker>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Worker worker : list) {
				if ((firmId != worker.getFirmId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_WORKER_WHERE);

			query.append(_FINDER_COLUMN_FIRMID_FIRMID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			else {
				query.append(WorkerModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(firmId);

				list = (List<Worker>)QueryUtil.list(q, getDialect(), start, end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first worker in the ordered set where firmId = &#63;.
	 *
	 * @param firmId the firm ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching worker
	 * @throws it.unisef.managesales.NoSuchWorkerException if a matching worker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Worker findByFirmId_First(long firmId,
		OrderByComparator orderByComparator)
		throws NoSuchWorkerException, SystemException {
		Worker worker = fetchByFirmId_First(firmId, orderByComparator);

		if (worker != null) {
			return worker;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("firmId=");
		msg.append(firmId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWorkerException(msg.toString());
	}

	/**
	 * Returns the first worker in the ordered set where firmId = &#63;.
	 *
	 * @param firmId the firm ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching worker, or <code>null</code> if a matching worker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Worker fetchByFirmId_First(long firmId,
		OrderByComparator orderByComparator) throws SystemException {
		List<Worker> list = findByFirmId(firmId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last worker in the ordered set where firmId = &#63;.
	 *
	 * @param firmId the firm ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching worker
	 * @throws it.unisef.managesales.NoSuchWorkerException if a matching worker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Worker findByFirmId_Last(long firmId,
		OrderByComparator orderByComparator)
		throws NoSuchWorkerException, SystemException {
		Worker worker = fetchByFirmId_Last(firmId, orderByComparator);

		if (worker != null) {
			return worker;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("firmId=");
		msg.append(firmId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchWorkerException(msg.toString());
	}

	/**
	 * Returns the last worker in the ordered set where firmId = &#63;.
	 *
	 * @param firmId the firm ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching worker, or <code>null</code> if a matching worker could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Worker fetchByFirmId_Last(long firmId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByFirmId(firmId);

		List<Worker> list = findByFirmId(firmId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the workers before and after the current worker in the ordered set where firmId = &#63;.
	 *
	 * @param workerId the primary key of the current worker
	 * @param firmId the firm ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next worker
	 * @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Worker[] findByFirmId_PrevAndNext(long workerId, long firmId,
		OrderByComparator orderByComparator)
		throws NoSuchWorkerException, SystemException {
		Worker worker = findByPrimaryKey(workerId);

		Session session = null;

		try {
			session = openSession();

			Worker[] array = new WorkerImpl[3];

			array[0] = getByFirmId_PrevAndNext(session, worker, firmId,
					orderByComparator, true);

			array[1] = worker;

			array[2] = getByFirmId_PrevAndNext(session, worker, firmId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Worker getByFirmId_PrevAndNext(Session session, Worker worker,
		long firmId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_WORKER_WHERE);

		query.append(_FINDER_COLUMN_FIRMID_FIRMID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		else {
			query.append(WorkerModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(firmId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(worker);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Worker> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the workers.
	 *
	 * @return the workers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Worker> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the workers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of workers
	 * @param end the upper bound of the range of workers (not inclusive)
	 * @return the range of workers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Worker> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the workers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of workers
	 * @param end the upper bound of the range of workers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of workers
	 * @throws SystemException if a system exception occurred
	 */
	public List<Worker> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Worker> list = (List<Worker>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_WORKER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_WORKER.concat(WorkerModelImpl.ORDER_BY_JPQL);
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Worker>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Worker>)QueryUtil.list(q, getDialect(), start,
							end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the workers where companyId = &#63; and groupId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByC_G(long companyId, long groupId)
		throws SystemException {
		for (Worker worker : findByC_G(companyId, groupId)) {
			remove(worker);
		}
	}

	/**
	 * Removes all the workers where firmId = &#63; from the database.
	 *
	 * @param firmId the firm ID
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByFirmId(long firmId) throws SystemException {
		for (Worker worker : findByFirmId(firmId)) {
			remove(worker);
		}
	}

	/**
	 * Removes all the workers from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Worker worker : findAll()) {
			remove(worker);
		}
	}

	/**
	 * Returns the number of workers where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching workers
	 * @throws SystemException if a system exception occurred
	 */
	public int countByC_G(long companyId, long groupId)
		throws SystemException {
		Object[] finderArgs = new Object[] { companyId, groupId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_C_G,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_WORKER_WHERE);

			query.append(_FINDER_COLUMN_C_G_COMPANYID_2);

			query.append(_FINDER_COLUMN_C_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_C_G, finderArgs,
					count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of workers that the user has permission to view where companyId = &#63; and groupId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param groupId the group ID
	 * @return the number of matching workers that the user has permission to view
	 * @throws SystemException if a system exception occurred
	 */
	public int filterCountByC_G(long companyId, long groupId)
		throws SystemException {
		if (!InlineSQLHelperUtil.isEnabled(groupId)) {
			return countByC_G(companyId, groupId);
		}

		StringBundler query = new StringBundler(3);

		query.append(_FILTER_SQL_COUNT_WORKER_WHERE);

		query.append(_FINDER_COLUMN_C_G_COMPANYID_2);

		query.append(_FINDER_COLUMN_C_G_GROUPID_2);

		String sql = InlineSQLHelperUtil.replacePermissionCheck(query.toString(),
				Worker.class.getName(), _FILTER_ENTITY_TABLE_FILTER_PK_COLUMN,
				groupId);

		Session session = null;

		try {
			session = openSession();

			SQLQuery q = session.createSQLQuery(sql);

			q.addScalar(COUNT_COLUMN_NAME,
				com.liferay.portal.kernel.dao.orm.Type.LONG);

			QueryPos qPos = QueryPos.getInstance(q);

			qPos.add(companyId);

			qPos.add(groupId);

			Long count = (Long)q.uniqueResult();

			return count.intValue();
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	/**
	 * Returns the number of workers where firmId = &#63;.
	 *
	 * @param firmId the firm ID
	 * @return the number of matching workers
	 * @throws SystemException if a system exception occurred
	 */
	public int countByFirmId(long firmId) throws SystemException {
		Object[] finderArgs = new Object[] { firmId };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_FIRMID,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_WORKER_WHERE);

			query.append(_FINDER_COLUMN_FIRMID_FIRMID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(firmId);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_FIRMID,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of workers.
	 *
	 * @return the number of workers
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_WORKER);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the worker persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.unisef.managesales.model.Worker")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Worker>> listenersList = new ArrayList<ModelListener<Worker>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<Worker>)InstanceFactory.newInstance(
							listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(WorkerImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = FirmPersistence.class)
	protected FirmPersistence firmPersistence;
	@BeanReference(type = WorkerPersistence.class)
	protected WorkerPersistence workerPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_WORKER = "SELECT worker FROM Worker worker";
	private static final String _SQL_SELECT_WORKER_WHERE = "SELECT worker FROM Worker worker WHERE ";
	private static final String _SQL_COUNT_WORKER = "SELECT COUNT(worker) FROM Worker worker";
	private static final String _SQL_COUNT_WORKER_WHERE = "SELECT COUNT(worker) FROM Worker worker WHERE ";
	private static final String _FINDER_COLUMN_C_G_COMPANYID_2 = "worker.companyId = ? AND ";
	private static final String _FINDER_COLUMN_C_G_GROUPID_2 = "worker.groupId = ?";
	private static final String _FINDER_COLUMN_FIRMID_FIRMID_2 = "worker.firmId = ?";
	private static final String _FILTER_ENTITY_TABLE_FILTER_PK_COLUMN = "worker.workerId";
	private static final String _FILTER_SQL_SELECT_WORKER_WHERE = "SELECT DISTINCT {worker.*} FROM MS_Worker worker WHERE ";
	private static final String _FILTER_SQL_SELECT_WORKER_NO_INLINE_DISTINCT_WHERE_1 =
		"SELECT {MS_Worker.*} FROM (SELECT DISTINCT worker.workerId FROM MS_Worker worker WHERE ";
	private static final String _FILTER_SQL_SELECT_WORKER_NO_INLINE_DISTINCT_WHERE_2 =
		") TEMP_TABLE INNER JOIN MS_Worker ON TEMP_TABLE.workerId = MS_Worker.workerId";
	private static final String _FILTER_SQL_COUNT_WORKER_WHERE = "SELECT COUNT(DISTINCT worker.workerId) AS COUNT_VALUE FROM MS_Worker worker WHERE ";
	private static final String _FILTER_ENTITY_ALIAS = "worker";
	private static final String _FILTER_ENTITY_TABLE = "MS_Worker";
	private static final String _ORDER_BY_ENTITY_ALIAS = "worker.";
	private static final String _ORDER_BY_ENTITY_TABLE = "MS_Worker.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Worker exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Worker exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(WorkerPersistenceImpl.class);
	private static Worker _nullWorker = new WorkerImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Worker> toCacheModel() {
				return _nullWorkerCacheModel;
			}
		};

	private static CacheModel<Worker> _nullWorkerCacheModel = new CacheModel<Worker>() {
			public Worker toEntityModel() {
				return _nullWorker;
			}
		};
}