package it.unisef.managesales.util;

public interface WebKeys extends com.liferay.portal.kernel.util.WebKeys {

		public static final String FIRM = "FIRM";
		public static final String FIRM_NAME = "FIRM_NAME";

		public static final String WORKER = "WORKER";

}
