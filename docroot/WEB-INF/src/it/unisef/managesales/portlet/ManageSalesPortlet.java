package it.unisef.managesales.portlet;

import it.unisef.managesales.model.Firm;
import it.unisef.managesales.model.Worker;
import it.unisef.managesales.service.FirmLocalServiceUtil;
import it.unisef.managesales.service.WorkerLocalServiceUtil;
import it.unisef.managesales.util.WebKeys;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class ManageSalesPortlet extends MVCPortlet {


	public void updateFirm(
			ActionRequest request, ActionResponse response)
		throws Exception {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
										Firm.class.getName(), request);

		long firmId = ParamUtil.getLong(request, "firmId");
		String name = ParamUtil.getString(request, "name");
		String address = ParamUtil.getString(request, "address");

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
										WebKeys.THEME_DISPLAY);

		long userId = themeDisplay.getUserId();

		if(firmId > 0 ) {
			FirmLocalServiceUtil.updateFirm(firmId, name, address,
					serviceContext);
		}
		else {
			FirmLocalServiceUtil.addFirm(
					userId, name, address, serviceContext);
		}

	}


	public void updateWorker(
			ActionRequest request, ActionResponse response)
		throws Exception {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
										Worker.class.getName(), request);

		long workerId = ParamUtil.getLong(request, "workerId");
		long firmId = ParamUtil.getLong(request, "firmId");

		String firstName = ParamUtil.getString(request, "firstName");
		String lastName = ParamUtil.getString(request, "lastName");

		String code = ParamUtil.getString(request, "code");

		/* "employmentDateMonth" generato automaticamente dalla taglib in edit_*.jsp
		 *  (nomecampo + Day/Month/Year) */

		int employmentDateMonth = ParamUtil.getInteger(request,
												"employmentDateMonth");
		int employmentDateDay = ParamUtil.getInteger(request,
												"employmentDateDay");
		int employmentDateYear = ParamUtil.getInteger(request,
												"employmentDateYear");

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
										WebKeys.THEME_DISPLAY);

		long userId = themeDisplay.getUserId();

		if(workerId > 0 ) {
			WorkerLocalServiceUtil.updateWorker(workerId, firstName, lastName,
					code, firmId, employmentDateMonth, employmentDateDay,
					employmentDateYear,
					serviceContext);
		}
		else {
			WorkerLocalServiceUtil.addWorker(
					userId, firmId, firstName, lastName, code,
					employmentDateMonth, employmentDateDay, employmentDateYear,
					serviceContext);
		}

	}

	public void deleteWorker(ActionRequest request, ActionResponse response)
			throws Exception {

			long workerId = ParamUtil.getLong(request, "workerId");

			WorkerLocalServiceUtil.deleteWorker(workerId);
	}

	public void deleteFirmWorkers(ActionRequest request, ActionResponse response)
			throws Exception {

			long firmId = ParamUtil.getLong(request, "firmId");

			WorkerLocalServiceUtil.deleteWorkersByFirm(firmId);
	}


	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {

			try {
				Firm firm = null;
				Worker worker = null;

				long firmId = ParamUtil.getLong(request, "firmId");
				long workerId = ParamUtil.getLong(request, "workerId");

				if(firmId > 0) {
					firm = FirmLocalServiceUtil.getFirm(firmId);
					request.setAttribute(WebKeys.FIRM, firm);
					request.setAttribute(WebKeys.FIRM_NAME, firm.getName());
				}

				if(workerId > 0) {
					worker = WorkerLocalServiceUtil.getWorker(workerId);
					request.setAttribute(WebKeys.WORKER, worker);
				}

			}
			catch(Exception e ) {
				throw new PortletException();
			}

			_log.info("ManagerSalesPortlet.render()");

			// TODO Auto-generated method stub
			super.render(request, response);
	}

	public static Log _log = LogFactoryUtil.getLog(ManageSalesPortlet.class);
}
