create table MS_Firm (
	firmId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	groupId LONG,
	name VARCHAR(75) null,
	address VARCHAR(75) null
);

create table MS_Worker (
	workerId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	groupId LONG,
	firstName VARCHAR(75) null,
	lastName VARCHAR(75) null,
	firmId LONG,
	employmentDate DATE null,
	code_ VARCHAR(75) null
);