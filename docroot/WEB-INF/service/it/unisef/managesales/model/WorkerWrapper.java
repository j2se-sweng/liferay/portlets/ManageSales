/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Worker}.
 * </p>
 *
 * @author    Giulio Piemontese
 * @see       Worker
 * @generated
 */
public class WorkerWrapper implements Worker, ModelWrapper<Worker> {
	public WorkerWrapper(Worker worker) {
		_worker = worker;
	}

	public Class<?> getModelClass() {
		return Worker.class;
	}

	public String getModelClassName() {
		return Worker.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("workerId", getWorkerId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("groupId", getGroupId());
		attributes.put("firstName", getFirstName());
		attributes.put("lastName", getLastName());
		attributes.put("firmId", getFirmId());
		attributes.put("employmentDate", getEmploymentDate());
		attributes.put("code", getCode());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long workerId = (Long)attributes.get("workerId");

		if (workerId != null) {
			setWorkerId(workerId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		String firstName = (String)attributes.get("firstName");

		if (firstName != null) {
			setFirstName(firstName);
		}

		String lastName = (String)attributes.get("lastName");

		if (lastName != null) {
			setLastName(lastName);
		}

		Long firmId = (Long)attributes.get("firmId");

		if (firmId != null) {
			setFirmId(firmId);
		}

		Date employmentDate = (Date)attributes.get("employmentDate");

		if (employmentDate != null) {
			setEmploymentDate(employmentDate);
		}

		String code = (String)attributes.get("code");

		if (code != null) {
			setCode(code);
		}
	}

	/**
	* Returns the primary key of this worker.
	*
	* @return the primary key of this worker
	*/
	public long getPrimaryKey() {
		return _worker.getPrimaryKey();
	}

	/**
	* Sets the primary key of this worker.
	*
	* @param primaryKey the primary key of this worker
	*/
	public void setPrimaryKey(long primaryKey) {
		_worker.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the worker ID of this worker.
	*
	* @return the worker ID of this worker
	*/
	public long getWorkerId() {
		return _worker.getWorkerId();
	}

	/**
	* Sets the worker ID of this worker.
	*
	* @param workerId the worker ID of this worker
	*/
	public void setWorkerId(long workerId) {
		_worker.setWorkerId(workerId);
	}

	/**
	* Returns the company ID of this worker.
	*
	* @return the company ID of this worker
	*/
	public long getCompanyId() {
		return _worker.getCompanyId();
	}

	/**
	* Sets the company ID of this worker.
	*
	* @param companyId the company ID of this worker
	*/
	public void setCompanyId(long companyId) {
		_worker.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this worker.
	*
	* @return the user ID of this worker
	*/
	public long getUserId() {
		return _worker.getUserId();
	}

	/**
	* Sets the user ID of this worker.
	*
	* @param userId the user ID of this worker
	*/
	public void setUserId(long userId) {
		_worker.setUserId(userId);
	}

	/**
	* Returns the user uuid of this worker.
	*
	* @return the user uuid of this worker
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _worker.getUserUuid();
	}

	/**
	* Sets the user uuid of this worker.
	*
	* @param userUuid the user uuid of this worker
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_worker.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this worker.
	*
	* @return the user name of this worker
	*/
	public java.lang.String getUserName() {
		return _worker.getUserName();
	}

	/**
	* Sets the user name of this worker.
	*
	* @param userName the user name of this worker
	*/
	public void setUserName(java.lang.String userName) {
		_worker.setUserName(userName);
	}

	/**
	* Returns the create date of this worker.
	*
	* @return the create date of this worker
	*/
	public java.util.Date getCreateDate() {
		return _worker.getCreateDate();
	}

	/**
	* Sets the create date of this worker.
	*
	* @param createDate the create date of this worker
	*/
	public void setCreateDate(java.util.Date createDate) {
		_worker.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this worker.
	*
	* @return the modified date of this worker
	*/
	public java.util.Date getModifiedDate() {
		return _worker.getModifiedDate();
	}

	/**
	* Sets the modified date of this worker.
	*
	* @param modifiedDate the modified date of this worker
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_worker.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the group ID of this worker.
	*
	* @return the group ID of this worker
	*/
	public long getGroupId() {
		return _worker.getGroupId();
	}

	/**
	* Sets the group ID of this worker.
	*
	* @param groupId the group ID of this worker
	*/
	public void setGroupId(long groupId) {
		_worker.setGroupId(groupId);
	}

	/**
	* Returns the first name of this worker.
	*
	* @return the first name of this worker
	*/
	public java.lang.String getFirstName() {
		return _worker.getFirstName();
	}

	/**
	* Sets the first name of this worker.
	*
	* @param firstName the first name of this worker
	*/
	public void setFirstName(java.lang.String firstName) {
		_worker.setFirstName(firstName);
	}

	/**
	* Returns the last name of this worker.
	*
	* @return the last name of this worker
	*/
	public java.lang.String getLastName() {
		return _worker.getLastName();
	}

	/**
	* Sets the last name of this worker.
	*
	* @param lastName the last name of this worker
	*/
	public void setLastName(java.lang.String lastName) {
		_worker.setLastName(lastName);
	}

	/**
	* Returns the firm ID of this worker.
	*
	* @return the firm ID of this worker
	*/
	public long getFirmId() {
		return _worker.getFirmId();
	}

	/**
	* Sets the firm ID of this worker.
	*
	* @param firmId the firm ID of this worker
	*/
	public void setFirmId(long firmId) {
		_worker.setFirmId(firmId);
	}

	/**
	* Returns the employment date of this worker.
	*
	* @return the employment date of this worker
	*/
	public java.util.Date getEmploymentDate() {
		return _worker.getEmploymentDate();
	}

	/**
	* Sets the employment date of this worker.
	*
	* @param employmentDate the employment date of this worker
	*/
	public void setEmploymentDate(java.util.Date employmentDate) {
		_worker.setEmploymentDate(employmentDate);
	}

	/**
	* Returns the code of this worker.
	*
	* @return the code of this worker
	*/
	public java.lang.String getCode() {
		return _worker.getCode();
	}

	/**
	* Sets the code of this worker.
	*
	* @param code the code of this worker
	*/
	public void setCode(java.lang.String code) {
		_worker.setCode(code);
	}

	public boolean isNew() {
		return _worker.isNew();
	}

	public void setNew(boolean n) {
		_worker.setNew(n);
	}

	public boolean isCachedModel() {
		return _worker.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_worker.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _worker.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _worker.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_worker.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _worker.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_worker.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new WorkerWrapper((Worker)_worker.clone());
	}

	public int compareTo(it.unisef.managesales.model.Worker worker) {
		return _worker.compareTo(worker);
	}

	@Override
	public int hashCode() {
		return _worker.hashCode();
	}

	public com.liferay.portal.model.CacheModel<it.unisef.managesales.model.Worker> toCacheModel() {
		return _worker.toCacheModel();
	}

	public it.unisef.managesales.model.Worker toEscapedModel() {
		return new WorkerWrapper(_worker.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _worker.toString();
	}

	public java.lang.String toXmlString() {
		return _worker.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_worker.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Worker getWrappedWorker() {
		return _worker;
	}

	public Worker getWrappedModel() {
		return _worker;
	}

	public void resetOriginalValues() {
		_worker.resetOriginalValues();
	}

	private Worker _worker;
}