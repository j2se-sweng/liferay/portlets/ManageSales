/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Firm}.
 * </p>
 *
 * @author    Giulio Piemontese
 * @see       Firm
 * @generated
 */
public class FirmWrapper implements Firm, ModelWrapper<Firm> {
	public FirmWrapper(Firm firm) {
		_firm = firm;
	}

	public Class<?> getModelClass() {
		return Firm.class;
	}

	public String getModelClassName() {
		return Firm.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("firmId", getFirmId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("groupId", getGroupId());
		attributes.put("name", getName());
		attributes.put("address", getAddress());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long firmId = (Long)attributes.get("firmId");

		if (firmId != null) {
			setFirmId(firmId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String address = (String)attributes.get("address");

		if (address != null) {
			setAddress(address);
		}
	}

	/**
	* Returns the primary key of this firm.
	*
	* @return the primary key of this firm
	*/
	public long getPrimaryKey() {
		return _firm.getPrimaryKey();
	}

	/**
	* Sets the primary key of this firm.
	*
	* @param primaryKey the primary key of this firm
	*/
	public void setPrimaryKey(long primaryKey) {
		_firm.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the firm ID of this firm.
	*
	* @return the firm ID of this firm
	*/
	public long getFirmId() {
		return _firm.getFirmId();
	}

	/**
	* Sets the firm ID of this firm.
	*
	* @param firmId the firm ID of this firm
	*/
	public void setFirmId(long firmId) {
		_firm.setFirmId(firmId);
	}

	/**
	* Returns the company ID of this firm.
	*
	* @return the company ID of this firm
	*/
	public long getCompanyId() {
		return _firm.getCompanyId();
	}

	/**
	* Sets the company ID of this firm.
	*
	* @param companyId the company ID of this firm
	*/
	public void setCompanyId(long companyId) {
		_firm.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this firm.
	*
	* @return the user ID of this firm
	*/
	public long getUserId() {
		return _firm.getUserId();
	}

	/**
	* Sets the user ID of this firm.
	*
	* @param userId the user ID of this firm
	*/
	public void setUserId(long userId) {
		_firm.setUserId(userId);
	}

	/**
	* Returns the user uuid of this firm.
	*
	* @return the user uuid of this firm
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _firm.getUserUuid();
	}

	/**
	* Sets the user uuid of this firm.
	*
	* @param userUuid the user uuid of this firm
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_firm.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this firm.
	*
	* @return the user name of this firm
	*/
	public java.lang.String getUserName() {
		return _firm.getUserName();
	}

	/**
	* Sets the user name of this firm.
	*
	* @param userName the user name of this firm
	*/
	public void setUserName(java.lang.String userName) {
		_firm.setUserName(userName);
	}

	/**
	* Returns the create date of this firm.
	*
	* @return the create date of this firm
	*/
	public java.util.Date getCreateDate() {
		return _firm.getCreateDate();
	}

	/**
	* Sets the create date of this firm.
	*
	* @param createDate the create date of this firm
	*/
	public void setCreateDate(java.util.Date createDate) {
		_firm.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this firm.
	*
	* @return the modified date of this firm
	*/
	public java.util.Date getModifiedDate() {
		return _firm.getModifiedDate();
	}

	/**
	* Sets the modified date of this firm.
	*
	* @param modifiedDate the modified date of this firm
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_firm.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the group ID of this firm.
	*
	* @return the group ID of this firm
	*/
	public long getGroupId() {
		return _firm.getGroupId();
	}

	/**
	* Sets the group ID of this firm.
	*
	* @param groupId the group ID of this firm
	*/
	public void setGroupId(long groupId) {
		_firm.setGroupId(groupId);
	}

	/**
	* Returns the name of this firm.
	*
	* @return the name of this firm
	*/
	public java.lang.String getName() {
		return _firm.getName();
	}

	/**
	* Sets the name of this firm.
	*
	* @param name the name of this firm
	*/
	public void setName(java.lang.String name) {
		_firm.setName(name);
	}

	/**
	* Returns the address of this firm.
	*
	* @return the address of this firm
	*/
	public java.lang.String getAddress() {
		return _firm.getAddress();
	}

	/**
	* Sets the address of this firm.
	*
	* @param address the address of this firm
	*/
	public void setAddress(java.lang.String address) {
		_firm.setAddress(address);
	}

	public boolean isNew() {
		return _firm.isNew();
	}

	public void setNew(boolean n) {
		_firm.setNew(n);
	}

	public boolean isCachedModel() {
		return _firm.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_firm.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _firm.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _firm.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_firm.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _firm.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_firm.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new FirmWrapper((Firm)_firm.clone());
	}

	public int compareTo(it.unisef.managesales.model.Firm firm) {
		return _firm.compareTo(firm);
	}

	@Override
	public int hashCode() {
		return _firm.hashCode();
	}

	public com.liferay.portal.model.CacheModel<it.unisef.managesales.model.Firm> toCacheModel() {
		return _firm.toCacheModel();
	}

	public it.unisef.managesales.model.Firm toEscapedModel() {
		return new FirmWrapper(_firm.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _firm.toString();
	}

	public java.lang.String toXmlString() {
		return _firm.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_firm.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Firm getWrappedFirm() {
		return _firm;
	}

	public Firm getWrappedModel() {
		return _firm;
	}

	public void resetOriginalValues() {
		_firm.resetOriginalValues();
	}

	private Firm _firm;
}