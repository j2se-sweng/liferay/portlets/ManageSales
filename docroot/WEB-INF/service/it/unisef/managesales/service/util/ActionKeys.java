package it.unisef.managesales.service.util;

public class ActionKeys extends  com.liferay.portal.security.permission.ActionKeys {


	public static final String ADD_FIRM = "ADD_FIRM";

//	public static final String UPDATE_FIRM= "UPDATE_FIRM";
//
//	public static final String DELETE_FIRM= "DELETE_FIRM";

	public static final String ADD_WORKER = "ADD_WORKER";
}
