/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link FirmService}.
 * </p>
 *
 * @author    Giulio Piemontese
 * @see       FirmService
 * @generated
 */
public class FirmServiceWrapper implements FirmService,
	ServiceWrapper<FirmService> {
	public FirmServiceWrapper(FirmService firmService) {
		_firmService = firmService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _firmService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_firmService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _firmService.invokeMethod(name, parameterTypes, arguments);
	}

	public it.unisef.managesales.model.Firm addFirm(long userId,
		java.lang.String name, java.lang.String address,
		com.liferay.portal.service.ServiceContext context)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _firmService.addFirm(userId, name, address, context);
	}

	public it.unisef.managesales.model.Firm updateFirm(long firmId,
		java.lang.String name, java.lang.String address,
		com.liferay.portal.service.ServiceContext context)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _firmService.updateFirm(firmId, name, address, context);
	}

	public it.unisef.managesales.model.Firm deleteFirm(long firmId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _firmService.deleteFirm(firmId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public FirmService getWrappedFirmService() {
		return _firmService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedFirmService(FirmService firmService) {
		_firmService = firmService;
	}

	public FirmService getWrappedService() {
		return _firmService;
	}

	public void setWrappedService(FirmService firmService) {
		_firmService = firmService;
	}

	private FirmService _firmService;
}