/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link WorkerService}.
 * </p>
 *
 * @author    Giulio Piemontese
 * @see       WorkerService
 * @generated
 */
public class WorkerServiceWrapper implements WorkerService,
	ServiceWrapper<WorkerService> {
	public WorkerServiceWrapper(WorkerService workerService) {
		_workerService = workerService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _workerService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_workerService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _workerService.invokeMethod(name, parameterTypes, arguments);
	}

	public it.unisef.managesales.model.Worker addWorker(long userId,
		long firmId, java.lang.String firstName, java.lang.String lastName,
		java.lang.String code, int employmentDateMonth, int employmentDateDay,
		int employmentDateYear,
		com.liferay.portal.service.ServiceContext context)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _workerService.addWorker(userId, firmId, firstName, lastName,
			code, employmentDateMonth, employmentDateDay, employmentDateYear,
			context);
	}

	public it.unisef.managesales.model.Worker updateWorker(long workerId,
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String code, long firmId, int employmentDateMonth,
		int employmentDateDay, int employmentDateYear,
		com.liferay.portal.service.ServiceContext context)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _workerService.updateWorker(workerId, firstName, lastName, code,
			firmId, employmentDateMonth, employmentDateDay, employmentDateYear,
			context);
	}

	public it.unisef.managesales.model.Worker deleteWorker(long workerId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _workerService.deleteWorker(workerId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public WorkerService getWrappedWorkerService() {
		return _workerService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedWorkerService(WorkerService workerService) {
		_workerService = workerService;
	}

	public WorkerService getWrappedService() {
		return _workerService;
	}

	public void setWrappedService(WorkerService workerService) {
		_workerService = workerService;
	}

	private WorkerService _workerService;
}