/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the worker local service. This utility wraps {@link it.unisef.managesales.service.impl.WorkerLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Giulio Piemontese
 * @see WorkerLocalService
 * @see it.unisef.managesales.service.base.WorkerLocalServiceBaseImpl
 * @see it.unisef.managesales.service.impl.WorkerLocalServiceImpl
 * @generated
 */
public class WorkerLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.unisef.managesales.service.impl.WorkerLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the worker to the database. Also notifies the appropriate model listeners.
	*
	* @param worker the worker
	* @return the worker that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker addWorker(
		it.unisef.managesales.model.Worker worker)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addWorker(worker);
	}

	/**
	* Creates a new worker with the primary key. Does not add the worker to the database.
	*
	* @param workerId the primary key for the new worker
	* @return the new worker
	*/
	public static it.unisef.managesales.model.Worker createWorker(long workerId) {
		return getService().createWorker(workerId);
	}

	/**
	* Deletes the worker with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param workerId the primary key of the worker
	* @return the worker that was removed
	* @throws PortalException if a worker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker deleteWorker(long workerId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteWorker(workerId);
	}

	/**
	* Deletes the worker from the database. Also notifies the appropriate model listeners.
	*
	* @param worker the worker
	* @return the worker that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker deleteWorker(
		it.unisef.managesales.model.Worker worker)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteWorker(worker);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static it.unisef.managesales.model.Worker fetchWorker(long workerId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchWorker(workerId);
	}

	/**
	* Returns the worker with the primary key.
	*
	* @param workerId the primary key of the worker
	* @return the worker
	* @throws PortalException if a worker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker getWorker(long workerId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getWorker(workerId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the workers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @return the range of workers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Worker> getWorkers(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getWorkers(start, end);
	}

	/**
	* Returns the number of workers.
	*
	* @return the number of workers
	* @throws SystemException if a system exception occurred
	*/
	public static int getWorkersCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getWorkersCount();
	}

	/**
	* Updates the worker in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param worker the worker
	* @return the worker that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker updateWorker(
		it.unisef.managesales.model.Worker worker)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateWorker(worker);
	}

	/**
	* Updates the worker in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param worker the worker
	* @param merge whether to merge the worker with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the worker that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker updateWorker(
		it.unisef.managesales.model.Worker worker, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateWorker(worker, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static it.unisef.managesales.model.Worker addWorker(long userId,
		long firmId, java.lang.String firstName, java.lang.String lastName,
		java.lang.String code, int employmentDateMonth, int employmentDateDay,
		int employmentDateYear,
		com.liferay.portal.service.ServiceContext context)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .addWorker(userId, firmId, firstName, lastName, code,
			employmentDateMonth, employmentDateDay, employmentDateYear, context);
	}

	public static it.unisef.managesales.model.Worker updateWorker(
		long workerId, java.lang.String firstName, java.lang.String lastName,
		java.lang.String code, long firmId, int employmentDateMonth,
		int employmentDateDay, int employmentDateYear,
		com.liferay.portal.service.ServiceContext context)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .updateWorker(workerId, firstName, lastName, code, firmId,
			employmentDateMonth, employmentDateDay, employmentDateYear, context);
	}

	public static java.util.List<it.unisef.managesales.model.Worker> getWorkers(
		long companyId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getWorkers(companyId, groupId, start, end);
	}

	public static int getWorkersCount(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getWorkersCount(companyId, groupId);
	}

	public static void deleteWorkersByFirm(long firmId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getService().deleteWorkersByFirm(firmId);
	}

	public static void clearService() {
		_service = null;
	}

	public static WorkerLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					WorkerLocalService.class.getName());

			if (invokableLocalService instanceof WorkerLocalService) {
				_service = (WorkerLocalService)invokableLocalService;
			}
			else {
				_service = new WorkerLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(WorkerLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(WorkerLocalService service) {
	}

	private static WorkerLocalService _service;
}