/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the firm local service. This utility wraps {@link it.unisef.managesales.service.impl.FirmLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Giulio Piemontese
 * @see FirmLocalService
 * @see it.unisef.managesales.service.base.FirmLocalServiceBaseImpl
 * @see it.unisef.managesales.service.impl.FirmLocalServiceImpl
 * @generated
 */
public class FirmLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.unisef.managesales.service.impl.FirmLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the firm to the database. Also notifies the appropriate model listeners.
	*
	* @param firm the firm
	* @return the firm that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm addFirm(
		it.unisef.managesales.model.Firm firm)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addFirm(firm);
	}

	/**
	* Creates a new firm with the primary key. Does not add the firm to the database.
	*
	* @param firmId the primary key for the new firm
	* @return the new firm
	*/
	public static it.unisef.managesales.model.Firm createFirm(long firmId) {
		return getService().createFirm(firmId);
	}

	/**
	* Deletes the firm with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param firmId the primary key of the firm
	* @return the firm that was removed
	* @throws PortalException if a firm with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm deleteFirm(long firmId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteFirm(firmId);
	}

	/**
	* Deletes the firm from the database. Also notifies the appropriate model listeners.
	*
	* @param firm the firm
	* @return the firm that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm deleteFirm(
		it.unisef.managesales.model.Firm firm)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteFirm(firm);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static it.unisef.managesales.model.Firm fetchFirm(long firmId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchFirm(firmId);
	}

	/**
	* Returns the firm with the primary key.
	*
	* @param firmId the primary key of the firm
	* @return the firm
	* @throws PortalException if a firm with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm getFirm(long firmId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getFirm(firmId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the firms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of firms
	* @param end the upper bound of the range of firms (not inclusive)
	* @return the range of firms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Firm> getFirms(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getFirms(start, end);
	}

	/**
	* Returns the number of firms.
	*
	* @return the number of firms
	* @throws SystemException if a system exception occurred
	*/
	public static int getFirmsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getFirmsCount();
	}

	/**
	* Updates the firm in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param firm the firm
	* @return the firm that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm updateFirm(
		it.unisef.managesales.model.Firm firm)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateFirm(firm);
	}

	/**
	* Updates the firm in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param firm the firm
	* @param merge whether to merge the firm with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the firm that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm updateFirm(
		it.unisef.managesales.model.Firm firm, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateFirm(firm, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<it.unisef.managesales.model.Firm> getFirms(
		long companyId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getFirms(companyId, groupId, start, end);
	}

	public static java.util.List<it.unisef.managesales.model.Firm> listFirms()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().listFirms();
	}

	public static java.util.List<it.unisef.managesales.model.Worker> getFirmWorkers(
		long firmId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getFirmWorkers(firmId);
	}

	public static int getFirmsCount(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getFirmsCount(companyId, groupId);
	}

	public static it.unisef.managesales.model.Firm addFirm(long userId,
		java.lang.String name, java.lang.String address,
		com.liferay.portal.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().addFirm(userId, name, address, serviceContext);
	}

	public static it.unisef.managesales.model.Firm updateFirm(long firmId,
		java.lang.String name, java.lang.String address,
		com.liferay.portal.service.ServiceContext context)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().updateFirm(firmId, name, address, context);
	}

	public static void deleteFirmWorkers(long firmId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		getService().deleteFirmWorkers(firmId);
	}

	public static void clearService() {
		_service = null;
	}

	public static FirmLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					FirmLocalService.class.getName());

			if (invokableLocalService instanceof FirmLocalService) {
				_service = (FirmLocalService)invokableLocalService;
			}
			else {
				_service = new FirmLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(FirmLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	public void setService(FirmLocalService service) {
	}

	private static FirmLocalService _service;
}