/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link FirmLocalService}.
 * </p>
 *
 * @author    Giulio Piemontese
 * @see       FirmLocalService
 * @generated
 */
public class FirmLocalServiceWrapper implements FirmLocalService,
	ServiceWrapper<FirmLocalService> {
	public FirmLocalServiceWrapper(FirmLocalService firmLocalService) {
		_firmLocalService = firmLocalService;
	}

	/**
	* Adds the firm to the database. Also notifies the appropriate model listeners.
	*
	* @param firm the firm
	* @return the firm that was added
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm addFirm(
		it.unisef.managesales.model.Firm firm)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.addFirm(firm);
	}

	/**
	* Creates a new firm with the primary key. Does not add the firm to the database.
	*
	* @param firmId the primary key for the new firm
	* @return the new firm
	*/
	public it.unisef.managesales.model.Firm createFirm(long firmId) {
		return _firmLocalService.createFirm(firmId);
	}

	/**
	* Deletes the firm with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param firmId the primary key of the firm
	* @return the firm that was removed
	* @throws PortalException if a firm with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm deleteFirm(long firmId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.deleteFirm(firmId);
	}

	/**
	* Deletes the firm from the database. Also notifies the appropriate model listeners.
	*
	* @param firm the firm
	* @return the firm that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm deleteFirm(
		it.unisef.managesales.model.Firm firm)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.deleteFirm(firm);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _firmLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.dynamicQueryCount(dynamicQuery);
	}

	public it.unisef.managesales.model.Firm fetchFirm(long firmId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.fetchFirm(firmId);
	}

	/**
	* Returns the firm with the primary key.
	*
	* @param firmId the primary key of the firm
	* @return the firm
	* @throws PortalException if a firm with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm getFirm(long firmId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.getFirm(firmId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the firms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of firms
	* @param end the upper bound of the range of firms (not inclusive)
	* @return the range of firms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Firm> getFirms(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.getFirms(start, end);
	}

	/**
	* Returns the number of firms.
	*
	* @return the number of firms
	* @throws SystemException if a system exception occurred
	*/
	public int getFirmsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.getFirmsCount();
	}

	/**
	* Updates the firm in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param firm the firm
	* @return the firm that was updated
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm updateFirm(
		it.unisef.managesales.model.Firm firm)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.updateFirm(firm);
	}

	/**
	* Updates the firm in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param firm the firm
	* @param merge whether to merge the firm with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the firm that was updated
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm updateFirm(
		it.unisef.managesales.model.Firm firm, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.updateFirm(firm, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _firmLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_firmLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _firmLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	public java.util.List<it.unisef.managesales.model.Firm> getFirms(
		long companyId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.getFirms(companyId, groupId, start, end);
	}

	public java.util.List<it.unisef.managesales.model.Firm> listFirms()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.listFirms();
	}

	public java.util.List<it.unisef.managesales.model.Worker> getFirmWorkers(
		long firmId) throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.getFirmWorkers(firmId);
	}

	public int getFirmsCount(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.getFirmsCount(companyId, groupId);
	}

	public it.unisef.managesales.model.Firm addFirm(long userId,
		java.lang.String name, java.lang.String address,
		com.liferay.portal.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.addFirm(userId, name, address, serviceContext);
	}

	public it.unisef.managesales.model.Firm updateFirm(long firmId,
		java.lang.String name, java.lang.String address,
		com.liferay.portal.service.ServiceContext context)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _firmLocalService.updateFirm(firmId, name, address, context);
	}

	public void deleteFirmWorkers(long firmId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		_firmLocalService.deleteFirmWorkers(firmId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public FirmLocalService getWrappedFirmLocalService() {
		return _firmLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedFirmLocalService(FirmLocalService firmLocalService) {
		_firmLocalService = firmLocalService;
	}

	public FirmLocalService getWrappedService() {
		return _firmLocalService;
	}

	public void setWrappedService(FirmLocalService firmLocalService) {
		_firmLocalService = firmLocalService;
	}

	private FirmLocalService _firmLocalService;
}