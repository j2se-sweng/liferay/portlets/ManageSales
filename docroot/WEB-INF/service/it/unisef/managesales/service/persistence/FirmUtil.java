/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.unisef.managesales.model.Firm;

import java.util.List;

/**
 * The persistence utility for the firm service. This utility wraps {@link FirmPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Giulio Piemontese
 * @see FirmPersistence
 * @see FirmPersistenceImpl
 * @generated
 */
public class FirmUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Firm firm) {
		getPersistence().clearCache(firm);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Firm> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Firm> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Firm> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Firm update(Firm firm, boolean merge)
		throws SystemException {
		return getPersistence().update(firm, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Firm update(Firm firm, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(firm, merge, serviceContext);
	}

	/**
	* Caches the firm in the entity cache if it is enabled.
	*
	* @param firm the firm
	*/
	public static void cacheResult(it.unisef.managesales.model.Firm firm) {
		getPersistence().cacheResult(firm);
	}

	/**
	* Caches the firms in the entity cache if it is enabled.
	*
	* @param firms the firms
	*/
	public static void cacheResult(
		java.util.List<it.unisef.managesales.model.Firm> firms) {
		getPersistence().cacheResult(firms);
	}

	/**
	* Creates a new firm with the primary key. Does not add the firm to the database.
	*
	* @param firmId the primary key for the new firm
	* @return the new firm
	*/
	public static it.unisef.managesales.model.Firm create(long firmId) {
		return getPersistence().create(firmId);
	}

	/**
	* Removes the firm with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param firmId the primary key of the firm
	* @return the firm that was removed
	* @throws it.unisef.managesales.NoSuchFirmException if a firm with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm remove(long firmId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchFirmException {
		return getPersistence().remove(firmId);
	}

	public static it.unisef.managesales.model.Firm updateImpl(
		it.unisef.managesales.model.Firm firm, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(firm, merge);
	}

	/**
	* Returns the firm with the primary key or throws a {@link it.unisef.managesales.NoSuchFirmException} if it could not be found.
	*
	* @param firmId the primary key of the firm
	* @return the firm
	* @throws it.unisef.managesales.NoSuchFirmException if a firm with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm findByPrimaryKey(long firmId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchFirmException {
		return getPersistence().findByPrimaryKey(firmId);
	}

	/**
	* Returns the firm with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param firmId the primary key of the firm
	* @return the firm, or <code>null</code> if a firm with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm fetchByPrimaryKey(
		long firmId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(firmId);
	}

	/**
	* Returns all the firms where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the matching firms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Firm> findByC_G(
		long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByC_G(companyId, groupId);
	}

	/**
	* Returns a range of all the firms where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of firms
	* @param end the upper bound of the range of firms (not inclusive)
	* @return the range of matching firms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Firm> findByC_G(
		long companyId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByC_G(companyId, groupId, start, end);
	}

	/**
	* Returns an ordered range of all the firms where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of firms
	* @param end the upper bound of the range of firms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching firms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Firm> findByC_G(
		long companyId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByC_G(companyId, groupId, start, end, orderByComparator);
	}

	/**
	* Returns the first firm in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching firm
	* @throws it.unisef.managesales.NoSuchFirmException if a matching firm could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm findByC_G_First(
		long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchFirmException {
		return getPersistence()
				   .findByC_G_First(companyId, groupId, orderByComparator);
	}

	/**
	* Returns the first firm in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching firm, or <code>null</code> if a matching firm could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm fetchByC_G_First(
		long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_G_First(companyId, groupId, orderByComparator);
	}

	/**
	* Returns the last firm in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching firm
	* @throws it.unisef.managesales.NoSuchFirmException if a matching firm could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm findByC_G_Last(
		long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchFirmException {
		return getPersistence()
				   .findByC_G_Last(companyId, groupId, orderByComparator);
	}

	/**
	* Returns the last firm in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching firm, or <code>null</code> if a matching firm could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm fetchByC_G_Last(
		long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_G_Last(companyId, groupId, orderByComparator);
	}

	/**
	* Returns the firms before and after the current firm in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param firmId the primary key of the current firm
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next firm
	* @throws it.unisef.managesales.NoSuchFirmException if a firm with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm[] findByC_G_PrevAndNext(
		long firmId, long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchFirmException {
		return getPersistence()
				   .findByC_G_PrevAndNext(firmId, companyId, groupId,
			orderByComparator);
	}

	/**
	* Returns all the firms that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the matching firms that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Firm> filterFindByC_G(
		long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().filterFindByC_G(companyId, groupId);
	}

	/**
	* Returns a range of all the firms that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of firms
	* @param end the upper bound of the range of firms (not inclusive)
	* @return the range of matching firms that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Firm> filterFindByC_G(
		long companyId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().filterFindByC_G(companyId, groupId, start, end);
	}

	/**
	* Returns an ordered range of all the firms that the user has permissions to view where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of firms
	* @param end the upper bound of the range of firms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching firms that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Firm> filterFindByC_G(
		long companyId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .filterFindByC_G(companyId, groupId, start, end,
			orderByComparator);
	}

	/**
	* Returns the firms before and after the current firm in the ordered set of firms that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* @param firmId the primary key of the current firm
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next firm
	* @throws it.unisef.managesales.NoSuchFirmException if a firm with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Firm[] filterFindByC_G_PrevAndNext(
		long firmId, long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchFirmException {
		return getPersistence()
				   .filterFindByC_G_PrevAndNext(firmId, companyId, groupId,
			orderByComparator);
	}

	/**
	* Returns all the firms.
	*
	* @return the firms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Firm> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the firms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of firms
	* @param end the upper bound of the range of firms (not inclusive)
	* @return the range of firms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Firm> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the firms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of firms
	* @param end the upper bound of the range of firms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of firms
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Firm> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the firms where companyId = &#63; and groupId = &#63; from the database.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByC_G(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByC_G(companyId, groupId);
	}

	/**
	* Removes all the firms from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of firms where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the number of matching firms
	* @throws SystemException if a system exception occurred
	*/
	public static int countByC_G(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByC_G(companyId, groupId);
	}

	/**
	* Returns the number of firms that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the number of matching firms that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public static int filterCountByC_G(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().filterCountByC_G(companyId, groupId);
	}

	/**
	* Returns the number of firms.
	*
	* @return the number of firms
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static FirmPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (FirmPersistence)PortletBeanLocatorUtil.locate(it.unisef.managesales.service.ClpSerializer.getServletContextName(),
					FirmPersistence.class.getName());

			ReferenceRegistry.registerReference(FirmUtil.class, "_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(FirmPersistence persistence) {
	}

	private static FirmPersistence _persistence;
}