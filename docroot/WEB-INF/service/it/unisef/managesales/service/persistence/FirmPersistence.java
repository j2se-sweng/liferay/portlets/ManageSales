/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.unisef.managesales.model.Firm;

/**
 * The persistence interface for the firm service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Giulio Piemontese
 * @see FirmPersistenceImpl
 * @see FirmUtil
 * @generated
 */
public interface FirmPersistence extends BasePersistence<Firm> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link FirmUtil} to access the firm persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the firm in the entity cache if it is enabled.
	*
	* @param firm the firm
	*/
	public void cacheResult(it.unisef.managesales.model.Firm firm);

	/**
	* Caches the firms in the entity cache if it is enabled.
	*
	* @param firms the firms
	*/
	public void cacheResult(
		java.util.List<it.unisef.managesales.model.Firm> firms);

	/**
	* Creates a new firm with the primary key. Does not add the firm to the database.
	*
	* @param firmId the primary key for the new firm
	* @return the new firm
	*/
	public it.unisef.managesales.model.Firm create(long firmId);

	/**
	* Removes the firm with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param firmId the primary key of the firm
	* @return the firm that was removed
	* @throws it.unisef.managesales.NoSuchFirmException if a firm with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm remove(long firmId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchFirmException;

	public it.unisef.managesales.model.Firm updateImpl(
		it.unisef.managesales.model.Firm firm, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the firm with the primary key or throws a {@link it.unisef.managesales.NoSuchFirmException} if it could not be found.
	*
	* @param firmId the primary key of the firm
	* @return the firm
	* @throws it.unisef.managesales.NoSuchFirmException if a firm with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm findByPrimaryKey(long firmId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchFirmException;

	/**
	* Returns the firm with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param firmId the primary key of the firm
	* @return the firm, or <code>null</code> if a firm with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm fetchByPrimaryKey(long firmId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the firms where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the matching firms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Firm> findByC_G(
		long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the firms where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of firms
	* @param end the upper bound of the range of firms (not inclusive)
	* @return the range of matching firms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Firm> findByC_G(
		long companyId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the firms where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of firms
	* @param end the upper bound of the range of firms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching firms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Firm> findByC_G(
		long companyId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first firm in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching firm
	* @throws it.unisef.managesales.NoSuchFirmException if a matching firm could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm findByC_G_First(long companyId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchFirmException;

	/**
	* Returns the first firm in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching firm, or <code>null</code> if a matching firm could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm fetchByC_G_First(long companyId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last firm in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching firm
	* @throws it.unisef.managesales.NoSuchFirmException if a matching firm could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm findByC_G_Last(long companyId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchFirmException;

	/**
	* Returns the last firm in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching firm, or <code>null</code> if a matching firm could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm fetchByC_G_Last(long companyId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the firms before and after the current firm in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param firmId the primary key of the current firm
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next firm
	* @throws it.unisef.managesales.NoSuchFirmException if a firm with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm[] findByC_G_PrevAndNext(
		long firmId, long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchFirmException;

	/**
	* Returns all the firms that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the matching firms that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Firm> filterFindByC_G(
		long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the firms that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of firms
	* @param end the upper bound of the range of firms (not inclusive)
	* @return the range of matching firms that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Firm> filterFindByC_G(
		long companyId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the firms that the user has permissions to view where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of firms
	* @param end the upper bound of the range of firms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching firms that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Firm> filterFindByC_G(
		long companyId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the firms before and after the current firm in the ordered set of firms that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* @param firmId the primary key of the current firm
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next firm
	* @throws it.unisef.managesales.NoSuchFirmException if a firm with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Firm[] filterFindByC_G_PrevAndNext(
		long firmId, long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchFirmException;

	/**
	* Returns all the firms.
	*
	* @return the firms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Firm> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the firms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of firms
	* @param end the upper bound of the range of firms (not inclusive)
	* @return the range of firms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Firm> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the firms.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of firms
	* @param end the upper bound of the range of firms (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of firms
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Firm> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the firms where companyId = &#63; and groupId = &#63; from the database.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByC_G(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the firms from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of firms where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the number of matching firms
	* @throws SystemException if a system exception occurred
	*/
	public int countByC_G(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of firms that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the number of matching firms that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public int filterCountByC_G(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of firms.
	*
	* @return the number of firms
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}