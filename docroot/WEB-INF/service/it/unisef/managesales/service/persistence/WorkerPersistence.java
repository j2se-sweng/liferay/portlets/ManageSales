/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.unisef.managesales.model.Worker;

/**
 * The persistence interface for the worker service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Giulio Piemontese
 * @see WorkerPersistenceImpl
 * @see WorkerUtil
 * @generated
 */
public interface WorkerPersistence extends BasePersistence<Worker> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link WorkerUtil} to access the worker persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the worker in the entity cache if it is enabled.
	*
	* @param worker the worker
	*/
	public void cacheResult(it.unisef.managesales.model.Worker worker);

	/**
	* Caches the workers in the entity cache if it is enabled.
	*
	* @param workers the workers
	*/
	public void cacheResult(
		java.util.List<it.unisef.managesales.model.Worker> workers);

	/**
	* Creates a new worker with the primary key. Does not add the worker to the database.
	*
	* @param workerId the primary key for the new worker
	* @return the new worker
	*/
	public it.unisef.managesales.model.Worker create(long workerId);

	/**
	* Removes the worker with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param workerId the primary key of the worker
	* @return the worker that was removed
	* @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Worker remove(long workerId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException;

	public it.unisef.managesales.model.Worker updateImpl(
		it.unisef.managesales.model.Worker worker, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the worker with the primary key or throws a {@link it.unisef.managesales.NoSuchWorkerException} if it could not be found.
	*
	* @param workerId the primary key of the worker
	* @return the worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Worker findByPrimaryKey(long workerId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException;

	/**
	* Returns the worker with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param workerId the primary key of the worker
	* @return the worker, or <code>null</code> if a worker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Worker fetchByPrimaryKey(long workerId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the workers where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the matching workers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Worker> findByC_G(
		long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the workers where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @return the range of matching workers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Worker> findByC_G(
		long companyId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the workers where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching workers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Worker> findByC_G(
		long companyId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first worker in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Worker findByC_G_First(long companyId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException;

	/**
	* Returns the first worker in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching worker, or <code>null</code> if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Worker fetchByC_G_First(long companyId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last worker in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Worker findByC_G_Last(long companyId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException;

	/**
	* Returns the last worker in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching worker, or <code>null</code> if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Worker fetchByC_G_Last(long companyId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the workers before and after the current worker in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param workerId the primary key of the current worker
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Worker[] findByC_G_PrevAndNext(
		long workerId, long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException;

	/**
	* Returns all the workers that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the matching workers that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Worker> filterFindByC_G(
		long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the workers that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @return the range of matching workers that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Worker> filterFindByC_G(
		long companyId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the workers that the user has permissions to view where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching workers that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Worker> filterFindByC_G(
		long companyId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the workers before and after the current worker in the ordered set of workers that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* @param workerId the primary key of the current worker
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Worker[] filterFindByC_G_PrevAndNext(
		long workerId, long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException;

	/**
	* Returns all the workers where firmId = &#63;.
	*
	* @param firmId the firm ID
	* @return the matching workers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Worker> findByFirmId(
		long firmId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the workers where firmId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param firmId the firm ID
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @return the range of matching workers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Worker> findByFirmId(
		long firmId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the workers where firmId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param firmId the firm ID
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching workers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Worker> findByFirmId(
		long firmId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first worker in the ordered set where firmId = &#63;.
	*
	* @param firmId the firm ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Worker findByFirmId_First(long firmId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException;

	/**
	* Returns the first worker in the ordered set where firmId = &#63;.
	*
	* @param firmId the firm ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching worker, or <code>null</code> if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Worker fetchByFirmId_First(long firmId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last worker in the ordered set where firmId = &#63;.
	*
	* @param firmId the firm ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Worker findByFirmId_Last(long firmId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException;

	/**
	* Returns the last worker in the ordered set where firmId = &#63;.
	*
	* @param firmId the firm ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching worker, or <code>null</code> if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Worker fetchByFirmId_Last(long firmId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the workers before and after the current worker in the ordered set where firmId = &#63;.
	*
	* @param workerId the primary key of the current worker
	* @param firmId the firm ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.unisef.managesales.model.Worker[] findByFirmId_PrevAndNext(
		long workerId, long firmId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException;

	/**
	* Returns all the workers.
	*
	* @return the workers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Worker> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the workers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @return the range of workers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Worker> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the workers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of workers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.unisef.managesales.model.Worker> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the workers where companyId = &#63; and groupId = &#63; from the database.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByC_G(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the workers where firmId = &#63; from the database.
	*
	* @param firmId the firm ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByFirmId(long firmId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the workers from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of workers where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the number of matching workers
	* @throws SystemException if a system exception occurred
	*/
	public int countByC_G(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of workers that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the number of matching workers that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public int filterCountByC_G(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of workers where firmId = &#63;.
	*
	* @param firmId the firm ID
	* @return the number of matching workers
	* @throws SystemException if a system exception occurred
	*/
	public int countByFirmId(long firmId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of workers.
	*
	* @return the number of workers
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}