/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.unisef.managesales.model.Worker;

import java.util.List;

/**
 * The persistence utility for the worker service. This utility wraps {@link WorkerPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Giulio Piemontese
 * @see WorkerPersistence
 * @see WorkerPersistenceImpl
 * @generated
 */
public class WorkerUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Worker worker) {
		getPersistence().clearCache(worker);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Worker> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Worker> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Worker> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Worker update(Worker worker, boolean merge)
		throws SystemException {
		return getPersistence().update(worker, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Worker update(Worker worker, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(worker, merge, serviceContext);
	}

	/**
	* Caches the worker in the entity cache if it is enabled.
	*
	* @param worker the worker
	*/
	public static void cacheResult(it.unisef.managesales.model.Worker worker) {
		getPersistence().cacheResult(worker);
	}

	/**
	* Caches the workers in the entity cache if it is enabled.
	*
	* @param workers the workers
	*/
	public static void cacheResult(
		java.util.List<it.unisef.managesales.model.Worker> workers) {
		getPersistence().cacheResult(workers);
	}

	/**
	* Creates a new worker with the primary key. Does not add the worker to the database.
	*
	* @param workerId the primary key for the new worker
	* @return the new worker
	*/
	public static it.unisef.managesales.model.Worker create(long workerId) {
		return getPersistence().create(workerId);
	}

	/**
	* Removes the worker with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param workerId the primary key of the worker
	* @return the worker that was removed
	* @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker remove(long workerId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException {
		return getPersistence().remove(workerId);
	}

	public static it.unisef.managesales.model.Worker updateImpl(
		it.unisef.managesales.model.Worker worker, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(worker, merge);
	}

	/**
	* Returns the worker with the primary key or throws a {@link it.unisef.managesales.NoSuchWorkerException} if it could not be found.
	*
	* @param workerId the primary key of the worker
	* @return the worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker findByPrimaryKey(
		long workerId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException {
		return getPersistence().findByPrimaryKey(workerId);
	}

	/**
	* Returns the worker with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param workerId the primary key of the worker
	* @return the worker, or <code>null</code> if a worker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker fetchByPrimaryKey(
		long workerId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(workerId);
	}

	/**
	* Returns all the workers where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the matching workers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Worker> findByC_G(
		long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByC_G(companyId, groupId);
	}

	/**
	* Returns a range of all the workers where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @return the range of matching workers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Worker> findByC_G(
		long companyId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByC_G(companyId, groupId, start, end);
	}

	/**
	* Returns an ordered range of all the workers where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching workers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Worker> findByC_G(
		long companyId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByC_G(companyId, groupId, start, end, orderByComparator);
	}

	/**
	* Returns the first worker in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker findByC_G_First(
		long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException {
		return getPersistence()
				   .findByC_G_First(companyId, groupId, orderByComparator);
	}

	/**
	* Returns the first worker in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching worker, or <code>null</code> if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker fetchByC_G_First(
		long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_G_First(companyId, groupId, orderByComparator);
	}

	/**
	* Returns the last worker in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker findByC_G_Last(
		long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException {
		return getPersistence()
				   .findByC_G_Last(companyId, groupId, orderByComparator);
	}

	/**
	* Returns the last worker in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching worker, or <code>null</code> if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker fetchByC_G_Last(
		long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByC_G_Last(companyId, groupId, orderByComparator);
	}

	/**
	* Returns the workers before and after the current worker in the ordered set where companyId = &#63; and groupId = &#63;.
	*
	* @param workerId the primary key of the current worker
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker[] findByC_G_PrevAndNext(
		long workerId, long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException {
		return getPersistence()
				   .findByC_G_PrevAndNext(workerId, companyId, groupId,
			orderByComparator);
	}

	/**
	* Returns all the workers that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the matching workers that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Worker> filterFindByC_G(
		long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().filterFindByC_G(companyId, groupId);
	}

	/**
	* Returns a range of all the workers that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @return the range of matching workers that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Worker> filterFindByC_G(
		long companyId, long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().filterFindByC_G(companyId, groupId, start, end);
	}

	/**
	* Returns an ordered range of all the workers that the user has permissions to view where companyId = &#63; and groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching workers that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Worker> filterFindByC_G(
		long companyId, long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .filterFindByC_G(companyId, groupId, start, end,
			orderByComparator);
	}

	/**
	* Returns the workers before and after the current worker in the ordered set of workers that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* @param workerId the primary key of the current worker
	* @param companyId the company ID
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker[] filterFindByC_G_PrevAndNext(
		long workerId, long companyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException {
		return getPersistence()
				   .filterFindByC_G_PrevAndNext(workerId, companyId, groupId,
			orderByComparator);
	}

	/**
	* Returns all the workers where firmId = &#63;.
	*
	* @param firmId the firm ID
	* @return the matching workers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Worker> findByFirmId(
		long firmId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByFirmId(firmId);
	}

	/**
	* Returns a range of all the workers where firmId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param firmId the firm ID
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @return the range of matching workers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Worker> findByFirmId(
		long firmId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByFirmId(firmId, start, end);
	}

	/**
	* Returns an ordered range of all the workers where firmId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param firmId the firm ID
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching workers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Worker> findByFirmId(
		long firmId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByFirmId(firmId, start, end, orderByComparator);
	}

	/**
	* Returns the first worker in the ordered set where firmId = &#63;.
	*
	* @param firmId the firm ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker findByFirmId_First(
		long firmId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException {
		return getPersistence().findByFirmId_First(firmId, orderByComparator);
	}

	/**
	* Returns the first worker in the ordered set where firmId = &#63;.
	*
	* @param firmId the firm ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching worker, or <code>null</code> if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker fetchByFirmId_First(
		long firmId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByFirmId_First(firmId, orderByComparator);
	}

	/**
	* Returns the last worker in the ordered set where firmId = &#63;.
	*
	* @param firmId the firm ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker findByFirmId_Last(
		long firmId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException {
		return getPersistence().findByFirmId_Last(firmId, orderByComparator);
	}

	/**
	* Returns the last worker in the ordered set where firmId = &#63;.
	*
	* @param firmId the firm ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching worker, or <code>null</code> if a matching worker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker fetchByFirmId_Last(
		long firmId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByFirmId_Last(firmId, orderByComparator);
	}

	/**
	* Returns the workers before and after the current worker in the ordered set where firmId = &#63;.
	*
	* @param workerId the primary key of the current worker
	* @param firmId the firm ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next worker
	* @throws it.unisef.managesales.NoSuchWorkerException if a worker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.unisef.managesales.model.Worker[] findByFirmId_PrevAndNext(
		long workerId, long firmId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.unisef.managesales.NoSuchWorkerException {
		return getPersistence()
				   .findByFirmId_PrevAndNext(workerId, firmId, orderByComparator);
	}

	/**
	* Returns all the workers.
	*
	* @return the workers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Worker> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the workers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @return the range of workers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Worker> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the workers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of workers
	* @param end the upper bound of the range of workers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of workers
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.unisef.managesales.model.Worker> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the workers where companyId = &#63; and groupId = &#63; from the database.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByC_G(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByC_G(companyId, groupId);
	}

	/**
	* Removes all the workers where firmId = &#63; from the database.
	*
	* @param firmId the firm ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByFirmId(long firmId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByFirmId(firmId);
	}

	/**
	* Removes all the workers from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of workers where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the number of matching workers
	* @throws SystemException if a system exception occurred
	*/
	public static int countByC_G(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByC_G(companyId, groupId);
	}

	/**
	* Returns the number of workers that the user has permission to view where companyId = &#63; and groupId = &#63;.
	*
	* @param companyId the company ID
	* @param groupId the group ID
	* @return the number of matching workers that the user has permission to view
	* @throws SystemException if a system exception occurred
	*/
	public static int filterCountByC_G(long companyId, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().filterCountByC_G(companyId, groupId);
	}

	/**
	* Returns the number of workers where firmId = &#63;.
	*
	* @param firmId the firm ID
	* @return the number of matching workers
	* @throws SystemException if a system exception occurred
	*/
	public static int countByFirmId(long firmId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByFirmId(firmId);
	}

	/**
	* Returns the number of workers.
	*
	* @return the number of workers
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static WorkerPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (WorkerPersistence)PortletBeanLocatorUtil.locate(it.unisef.managesales.service.ClpSerializer.getServletContextName(),
					WorkerPersistence.class.getName());

			ReferenceRegistry.registerReference(WorkerUtil.class, "_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(WorkerPersistence persistence) {
	}

	private static WorkerPersistence _persistence;
}