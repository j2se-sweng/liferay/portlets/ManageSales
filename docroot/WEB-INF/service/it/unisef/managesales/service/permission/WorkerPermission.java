package it.unisef.managesales.service.permission;

import it.unisef.managesales.model.Worker;
import it.unisef.managesales.service.WorkerLocalServiceUtil;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.security.auth.PrincipalException;
import com.liferay.portal.security.permission.PermissionChecker;

/* Azioni sui singoli task */

public class WorkerPermission {

	public static void check(
			PermissionChecker permissionChecker, Worker worker,
			String actionId)
		throws PortalException {

		if (!contains(permissionChecker, worker, actionId)) {
			throw new PrincipalException();
		}
	}

	public static void check(
			PermissionChecker permissionChecker, long workerId, String actionId)
		throws PortalException, SystemException {

		if (!contains(permissionChecker, workerId, actionId)) {
			throw new PrincipalException();
		}
	}

	public static boolean contains(
		PermissionChecker permissionChecker, Worker worker,
		String actionId) {

		if (permissionChecker.hasOwnerPermission(
				worker.getCompanyId(), Worker.class.getName(),
				worker.getWorkerId(), worker.getUserId(), actionId)) {

			return true;
		}


		return permissionChecker.hasPermission(
			worker.getGroupId(), Worker.class.getName(), worker.getWorkerId(),
			actionId);
	}

	public static boolean contains(
			PermissionChecker permissionChecker, long workerId, String actionId)
		throws PortalException, SystemException {

		Worker worker = WorkerLocalServiceUtil.getWorker(workerId);

		return contains(permissionChecker, worker, actionId);
	}

}
