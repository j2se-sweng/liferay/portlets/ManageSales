Liferay.Service.register("Liferay.Service.MS", "it.unisef.managesales.service", "ManageSales-portlet");

Liferay.Service.registerClass(
	Liferay.Service.MS, "Firm",
	{
		addFirm: true,
		updateFirm: true,
		deleteFirm: true
	}
);

Liferay.Service.registerClass(
	Liferay.Service.MS, "Worker",
	{
		addWorker: true,
		updateWorker: true,
		deleteWorker: true
	}
);