<%@include file="/init.jsp" %>

<liferay-portlet:renderURL var="editWorkerURL">
		<liferay-portlet:param name="mvcPath" value="/edit_worker.jsp" />
		<liferay-portlet:param name="redirect" value="<%= currentURL %>" />
</liferay-portlet:renderURL>

<aui:button name="add" onClick="<%= editWorkerURL %>" value="add-worker" />

<liferay-ui:header title="view-workers" backURL="<%= redirect %>" />

<liferay-ui:search-container delta="5"  emptyResultsMessage="no-workers-were-found" >

	<liferay-ui:search-container-results >
		<%

// 			int start = QueryUtil.ALL_POS;
// 			int end = QueryUtil.ALL_POS;

			int start = searchContainer.getStart();
			int end = searchContainer.getEnd();

			if (permissionChecker.isCompanyAdmin()) {

				results = WorkerLocalServiceUtil.getWorkers(start, end);
				total = WorkerLocalServiceUtil.getWorkersCount(themeDisplay.getCompanyId(),
														themeDisplay.getScopeGroupId());
			}
			else {

				results = WorkerLocalServiceUtil.getWorkers(
						themeDisplay.getCompanyId(),
						themeDisplay.getScopeGroupId(),
						start,
						end);


				total = WorkerLocalServiceUtil.getWorkersCount(
						themeDisplay.getCompanyId(),
						themeDisplay.getScopeGroupId());

			}

			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);


			System.out.println("results per page = " + results.size());

		%>

	</liferay-ui:search-container-results>

		<!-- modelVar : Bean da cui prendere la proprietÓ specificata in keyProperty  -->

			<liferay-ui:search-container-row
				className="it.unisef.managesales.model.Worker"
				keyProperty="workerId"
				modelVar="worker"
			>

				<liferay-ui:search-container-column-text
					name="worker-id"
					property="workerId"
				/>

				<%
					User user_ = UserLocalServiceUtil.getUser(
									worker.getUserId());
					ExpandoBridge expandoBridge = user_.getExpandoBridge();
				%>

				<liferay-ui:search-container-column-text
					name="first-name"
					property="firstName"
				/>

				<liferay-ui:search-container-column-text
					name="last-name"
					property="lastName"
				/>

				<liferay-ui:search-container-column-text
					name="employment-date"
					value="<%= dateFormatDate.format(
									worker.getEmploymentDate()) %>"
				/>

				<liferay-ui:search-container-column-text
					name="firm-name"
					value="<%= FirmLocalServiceUtil.getFirm(
							worker.getFirmId()).getName() %>"
				/>

				<liferay-ui:search-container-column-jsp
					align="right"
					path="/worker_action.jsp"
				/>

			</liferay-ui:search-container-row>


	<liferay-ui:search-iterator paginate="false"/>

</liferay-ui:search-container>