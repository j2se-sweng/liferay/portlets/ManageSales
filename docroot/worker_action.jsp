<%@ include file="/init.jsp" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow"%>

<%
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Worker work = (Worker)row.getObject();

	long workerId = work.getWorkerId();
%>

<liferay-ui:icon-menu>

	<c:if test="<%= WorkerPermission.contains(permissionChecker, workerId,
				ActionKeys.UPDATE) %>">
		<portlet:renderURL var="updateWorkerURL">
			<portlet:param name="mvcPath" value="/edit_worker.jsp"/>
			<portlet:param name="redirect" value="<%= currentURL %>"/>
			<portlet:param name="workerId" value="<%= String.valueOf(workerId) %>"/>
		</portlet:renderURL>

		<liferay-ui:icon image="edit" url="<%= updateWorkerURL.toString() %>"/>
	</c:if>
	<c:if test="<%= FirmPermission.contains(permissionChecker, workerId,
			ActionKeys.PERMISSIONS) %>">
			<liferay-security:permissionsURL
				modelResource="it.unisef.managesales.model.Firm"
				modelResourceDescription="<%= HtmlUtil.escape(themeDisplay.getScopeGroupName()) %>"
				resourcePrimKey="<%= String.valueOf(scopeGroupId) %>"
				var="permissionsURL">
			</liferay-security:permissionsURL>
		<liferay-ui:icon image="permissions" url="<%= permissionsURL.toString() %>"/>
	</c:if>
	<c:if test="<%= WorkerPermission.contains(permissionChecker, workerId,
				ActionKeys.DELETE) %>">
		<portlet:actionURL name="deleteWorker" var="deleteWorkerURL">
			<portlet:param name="workerId" value="<%= String.valueOf(workerId) %>"/>
			<portlet:param name="redirect" value="<%= currentURL %>"/>
		</portlet:actionURL>

		<liferay-ui:icon image="delete" url="<%= deleteWorkerURL.toString() %>"/>
	</c:if>

</liferay-ui:icon-menu>