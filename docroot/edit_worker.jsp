<%@ include file="/init.jsp" %>


<%
	Worker worker = (Worker) request.getAttribute(WebKeys.WORKER);

%>

<liferay-ui:header title='<%= worker != null ? worker.getLastName() : "new-worker" %>' backURL="<%= redirect %>" />

<portlet:actionURL name="updateWorker" var="updateFirmURL" >
	<portlet:param name="mvcPath" value="/edit_worker.jsp" />
	<portlet:param name="redirect" value="<%= redirect %>" />
</portlet:actionURL>


<aui:form action="<%= updateFirmURL %>" method="post" name="fm" >
	<aui:fieldset>

		<aui:input type="hidden" name="redirect" value="<%= redirect %>" />
		<aui:input type="hidden" name="workerId" value="<%= worker == null ? StringPool.BLANK : worker.getWorkerId() %>" />

		<liferay-ui:error exception="<%= WorkerNameException.class %>" message="please-enter-a-valid-name" />
		<liferay-ui:error exception="<%= WorkerLastNameException.class %>" message="please-enter-a-valid-name" />

		<aui:model-context bean="<%= worker %>" model="<%= Worker.class %>" />

		<aui:input name="firstName" />

		<aui:input name="lastName" />

		<aui:select name="firmId" label="select-firm">
			<%
				List<Firm> allfirms = FirmLocalServiceUtil.listFirms();

				for(Firm firm : allfirms) {
					%>
						<aui:option value="<%= firm.getFirmId() %>">
							 <%= firm.getName() %>
						</aui:option>
					<%
				}
			%>
		</aui:select>

		<aui:input name="code" />

		<aui:input name="employmentDate" />

	</aui:fieldset>

	<aui:button-row>
		<aui:button type="submit" value="save" />

		<aui:button type="cancel" onClick="<%= redirect %>" />

	</aui:button-row>


</aui:form>