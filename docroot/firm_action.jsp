<%@ include file="/init.jsp" %>

<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow"%>

<%
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Firm firm = (Firm)row.getObject();

	long firmId = firm.getFirmId();
%>

<liferay-ui:icon-menu>
	<c:if test="<%= FirmPermission.contains(permissionChecker, firmId,
				ActionKeys.UPDATE) %>">
			<portlet:renderURL var="updateFirmURL">
				<portlet:param name="mvcPath" value="/edit_firm.jsp"/>
				<portlet:param name="redirect" value="<%= redirect %>"/>
			</portlet:renderURL>

		<liferay-ui:icon image="edit" url="<%= updateFirmURL.toString() %>"/>
	</c:if>
	<c:if test="<%= ManageSalesPermission.contains(permissionChecker, scopeGroupId,
			ActionKeys.PERMISSIONS) %>">
			<liferay-security:permissionsURL
				modelResource="it.unisef.managesales"
				modelResourceDescription="<%= HtmlUtil.escape(themeDisplay.getScopeGroupName()) %>"
				resourcePrimKey="<%= String.valueOf(scopeGroupId) %>"
				var="permissionsURL">
			</liferay-security:permissionsURL>
		<liferay-ui:icon image="permissions" url="<%= permissionsURL.toString() %>"/>
	</c:if>
	<c:if test="<%= FirmPermission.contains(permissionChecker, firmId,
				ActionKeys.DELETE) %>">
		<portlet:actionURL name="deleteFirm" var="deleteFirmURL">
			<portlet:param name="firmId" value="<%= String.valueOf(firmId) %>"/>
			<portlet:param name="redirect" value="<%= currentURL %>"/>
		</portlet:actionURL>

		<liferay-ui:icon image="delete" url="<%= deleteFirmURL.toString() %>"/>
	</c:if>
</liferay-ui:icon-menu>